// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Sprites/ColorSwap"
{
	Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _SwapTex("Color Data", 2D) = "transparent" {}
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="TransparentCutout" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}
		
		Cull Off
		ZWrite Off
		Blend Off
		
		CGPROGRAM

		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert alpha vertex:surfVert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0
		
		sampler2D _MainTex;
		sampler2D _SwapTex;

		struct Input
		{
			float4 vertex   : SV_POSITION;
			fixed4 color    : COLOR;
			half2 texcoord  : TEXCOORD0;
		};

		void surfVert (inout appdata_full v, out Input o)
		{	
			UNITY_INITIALIZE_OUTPUT(Input,o);
			o.vertex = UnityObjectToClipPos (v.vertex);
			o.texcoord = v.texcoord;
			o.color = v.color;
			#ifdef PIXELSNAP_ON
			o.vertex = UnityPixelSnap (o.vertex);
			#endif
		}

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 c = tex2D(_MainTex, IN.texcoord);
			fixed4 swapCol = tex2D(_SwapTex, float2(c.r, 0));
			fixed4 final = lerp(c, swapCol, swapCol.a)  * IN.color;
			o.Albedo = final.rgb * c.a;
			o.Alpha = c.a;
		}
		ENDCG

	}
}
