﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveManager {

    public static List<Game> GameList = new List<Game>();
    public static Game CurrentGame;

    public static void LoadList() {
        if (File.Exists(savePath)) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(savePath, FileMode.Open);
            GameList = (List<Game>) bf.Deserialize(file);
            file.Close();
        }
    }

    public static void SaveList() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(savePath);
        bf.Serialize(file, GameList);
        file.Close();
    }

    public static void DeleteList() {
        GameList = new List<Game>();
        File.Delete(savePath);
    }

    public static void SaveAndLoad() {
        SaveList();
        LoadList();
    }

    public static void SaveGame() {
        CurrentGame.fetchAllData();
        SaveAndLoad();
    }

    public static void LoadGame(Game game) {
        CurrentGame = game;
        M.zone.moveToZone(game.zone, game.spawn, () => {
            game.loadAllData();
            M.UI.closeUI();
        });
        
    }

    public static void NewGame() {
        Game game = new Game();
        GameList.Add(game);
        CurrentGame = game;
        SaveList();
    }

    public static void DeleteGame(Game game) {
        GameList.Remove(game);
        SaveAndLoad();
    }

    public static string savePath {
        get {
            return Application.persistentDataPath + "/saved_games.bin";
        }
    }
}

[System.Serializable]
public class Game {
    public string timeStamp;
    public string zone;
    public ZoneSpawnType spawn;
    public List<Data> data;

    public Game() {
        timeStamp = System.DateTime.Now.ToString();
    }

    public void fetchAllData() {
        zone = M.zone.currentZone;
        spawn = M.zone.zoneSpawn;

        data = new List<Data>();
        data.Add(new PlayerData());
        data.Add(new InventoryData());
        data.Add(new EquipmentData());
        data.Add(new StatData());
        data.Add(new LootChestData());
        data.Add(new ActionbarData());

        foreach (Data obj in data) {
            obj.fetchData();
        }
    }

    public void loadAllData() {
        if (data != null) {
            foreach (Data obj in data) {
                obj.loadData();
            }
            S.item.deselectAll();
        }
    }
}

[System.Serializable]
public abstract class Data {
    public abstract void fetchData();
    public abstract void loadData();
}

[System.Serializable]
public class PlayerData : Data {
    public List<int> completedQuestIds, acceptedQuestIds;

    public override void fetchData() {
        completedQuestIds = new List<int>();
        acceptedQuestIds = new List<int>();

        foreach (Quest quest in D.quest.getCompletedQuests()) {
            completedQuestIds.Add(quest.ID);
        }

        foreach (Quest quest in D.quest.getAcceptedQuests()) {
            acceptedQuestIds.Add(quest.ID);
        }
    }

    public override void loadData() {
        foreach (Quest quest in D.quest.getCompletedQuests()) {
            quest.isCompleted = false;
        }

        foreach (Quest quest in D.quest.getAcceptedQuests()) {
            quest.isAccepted = false;
        }

        if (completedQuestIds != null) {
            foreach (int id in completedQuestIds) {
                D.quest.findQuestById(id).isCompleted = true;
            }
        }

        if (acceptedQuestIds != null) {
            foreach (int id in acceptedQuestIds) {
                D.quest.findQuestById(id).isAccepted = true;
            }
        }

    }
}

[System.Serializable]
public class InventoryData : Data {
    public List<InventorySlotData> inventoryItems;

    public override void fetchData() {
        inventoryItems = new List<InventorySlotData>();

        List<InventorySlot> slots = M.inventory.inventorySlots;
        for (int i = 0; i < slots.Count; i++) {
            InventorySlotData data = new InventorySlotData();
            Item item = slots[i].item;
            if (item != null) {
                data.item = item.ID;
                data.slot = i;

                if (item.has<Stackable>()) {
                    Stackable stackable = item.get<Stackable>();
                    data.stack = stackable.amount;
                }

                inventoryItems.Add(data);
            }
        }
    }

    public override void loadData() {
        M.inventory.emptyInventory();
        foreach (InventorySlotData data in inventoryItems) {
            Item item = D.item.getItemById(data.item).Clone();
            InventorySlot slot = M.inventory.getSlotByIndex(data.slot);
            if (item.has<Stackable>()) {
                Stackable stackable = item.get<Stackable>();
                stackable.amount = data.stack;
            }
            slot.item = item;
        }
    }
}

[System.Serializable]
public class InventorySlotData {
    public int item;
    public int stack;
    public int slot;
}

[System.Serializable]
public class EquipmentData : Data {
    public List<int> equipmentItems;

    public override void fetchData() {
        equipmentItems = new List<int>();

        List<EquipmentSlot> slots = M.equipment.slots;
        for (int i = 0; i < slots.Count; i++) {
            Item item = slots[i].item;
            if (item != null) {
                equipmentItems.Add(item.ID);
            }
        }
    }

    public override void loadData() { 
        M.equipment.emptyEquipment();
        foreach (int itemID in equipmentItems) {
            EquipmentItem item = D.item.getItemById(itemID).Clone() as EquipmentItem;
            M.equipment.getEquipmentSlot(item.equipmentType).item = item;
        }
    }
}

[System.Serializable]
public class StatData : Data {
    public int gold;
    public int experience;
    public List<CombatStatData> stats;

    public override void fetchData() {
        gold = M.inventory.totalGold;
        experience = M.experience.totalXP;
        stats = new List<CombatStatData>();
        foreach(CombatStat stat in M.stat.combatStats) {
            stats.Add(new CombatStatData(stat.type, stat.currentValue, stat.totalValue));
        }
    }

    public override void loadData() { 
        M.inventory.totalGold = gold;
        M.experience.totalXP = experience;
        foreach(CombatStatData statData in stats) {
            CombatStat stat = M.stat.getStat(statData.type);
            stat.currentValue = statData.currentValue;
            stat.totalValue = statData.totalValue;
        }
    }
}

[System.Serializable]
public class CombatStatData {
	public CombatStatType type;
    public int currentValue;
    public int totalValue;

    public CombatStatData(CombatStatType _type, int _currentValue, int _totalValue) {
        type = _type;
        currentValue = _currentValue;
        totalValue = _totalValue;
    }
}

[System.Serializable]
public class LootChestData : Data {
    public List<ChestSave> chestSaves;

    public override void fetchData() {
        chestSaves = new List<ChestSave>();
        chestSaves = M.loot.chestSaves;
    }

    public override void loadData() { 
        M.loot.chestSaves = chestSaves;
    }
}

[System.Serializable]
public class ActionbarData : Data {
    public List<int> spells;

    public override void fetchData() {
        spells = new List<int>();
        foreach(PlayerSpell spell in M.actionbar.spells) {
            spells.Add(spell.ID);
        }
    }

    public override void loadData() { 
        M.actionbar.emptyActionbar();
        foreach(int id in spells) {
            PlayerSpell spell = D.spell.getSpellById(id) as PlayerSpell;
            M.spell.useSpell(spell);
        }
    }
}