﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpellSelect : MonoBehaviour {

	public PlayerSpell selectedspell;
    public ToggleGroup toggleGroup;
    public bool selected = true;

	public SpellStatList spellStatList;

    public Image icon;
    public Text title, description;

	public Button useBtn, removeBtn;

	void Awake(){
        useBtn.onClick.AddListener( delegate {
            useSpell();
        });

        removeBtn.onClick.AddListener( delegate {
            removeSpell();
        });

        toggleGroup = GetComponent<ToggleGroup>();
	}

    public void deselectAll() {
        toggleGroup.SetAllTogglesOff();
    }

	void LateUpdate(){

        if( toggleGroup.AnyTogglesOn() ) {
            Toggle toggle = toggleGroup.GetActive();
			selectedspell = toggle.GetComponent<SpellSlot>().spell;

			selected = (selectedspell != null);

			if (!selected)
				return;

            title.text = selectedspell.name + " (level " + selectedspell.level + ")";
            description.text = selectedspell.description;
			icon.sprite = selectedspell.sprite;

            if( selectedspell.isAction ) {
                removeBtn.gameObject.SetActive(true);
                useBtn.gameObject.SetActive(false);
            } else {
                removeBtn.gameObject.SetActive(false);
                useBtn.gameObject.SetActive(true);

                useBtn.interactable = !M.actionbar.isFull;
            }
        } else {
            selected = false;
        }
    }

    void useSpell() {
        M.spell.useSpell(selectedspell);
    }

    void removeSpell() {
        M.spell.removeSpell(selectedspell);
    }
}
