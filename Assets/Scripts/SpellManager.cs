﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class SpellManager : MonoBehaviour {

	public SpellUI spellUI;

	public void removeSpell(PlayerSpell spell){
		if( M.actionbar.removeSpell( spell ) ) {
			spellUI.spellList.getSpellBook();
			spellUI.spellList.findAndSelectSpell(spell);
		}
	}

	public void useSpell(PlayerSpell spell){
		if( M.actionbar.useSpell( spell ) ) {
			spellUI.spellList.getSpellBook();
			spellUI.spellList.findAndSelectSpell(spell);
		}
	}


    public bool canUseSpell( PlayerSpell spell ){
        Item mainHand = M.equipment.getEquipmentSlot(EquipmentType.Mainhand).item;
        if(mainHand == null) return false;   

        switch(spell.GetType().ToString()){
            case "MeleeSpell":
                return (mainHand is MeleeWeapon);
            case "RangeSpell":
                return (mainHand is RangeWeapon);
            case "MagicSpell":
                return (mainHand is MagicWeapon);
        }

        return false;
    }
}