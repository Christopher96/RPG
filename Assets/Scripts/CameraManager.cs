﻿using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

	ZoneEdge leftEdge, rightEdge;
	public ZoneBorder leftBorder, rightBorder;

	public Player player;

	public Vector3 playerPos;

	public float center;
	public float Xmax;
	public float Xmin;

	public bool onEdge;

	void Start() {
		center = Screen.width / 2;
	}

	public void updateEdges() {
		List<ZoneEdge> edges = new List<ZoneEdge>(FindObjectsOfType<ZoneEdge>());
		leftEdge = edges.Find(edge => edge.type == ZoneEdgeType.Left);
		rightEdge = edges.Find(edge => edge.type == ZoneEdgeType.Right);

		Xmin = (leftEdge.transform.position.x - leftEdge.col.bounds.extents.x);
		Xmax = (rightEdge.transform.position.x - rightEdge.col.bounds.extents.x);

		ZoneSwitch leftSwitch = leftEdge.GetComponent<ZoneSwitch>();
		leftBorder.col.isTrigger = (leftSwitch != null && leftSwitch.edgeType.Equals(ZoneEdgeType.Left));
		ZoneSwitch rightSwitch = rightEdge.GetComponent<ZoneSwitch>();
		rightBorder.col.isTrigger = (rightSwitch != null && rightSwitch.edgeType.Equals(ZoneEdgeType.Right));
	}

	public bool passed = false;

	public void updatePosition() {

		if( leftEdge == null || rightEdge == null) return;

		playerPos = player.transform.position;

		Vector3 cameraPos = Camera.main.transform.position;

		onEdge = false;
		
		if( leftBorder.transform.position.x < Xmin && playerPos.x < leftEdge.transform.position.x) {
			onEdge = true;
			cameraPos.x = leftEdge.transform.position.x;
		} else if ( rightBorder.transform.position.x > Xmax && playerPos.x > rightEdge.transform.position.x ) {
			onEdge = true;
			cameraPos.x = rightEdge.transform.position.x;
		} else if ( M.player.state == PlayerState.Entering ) {
			cameraPos.x += 0.019f;
		} else if( (M.player.state == PlayerState.Escaping || M.combat.isFinished) && !passed ) {
            if (cameraPos.x > playerPos.x)
                cameraPos.x -= 0.015f;
            else
                passed = true;
		} else if( !M.combat.inCombat || passed ){
			cameraPos.x = playerPos.x;
		}

		Camera.main.transform.position = cameraPos;
	}

	void FixedUpdate() {
		updatePosition();
	}
}
