﻿using UnityEngine;
using System.Collections.Generic;

public class QuestDatabase : MonoBehaviour {

	private int globalID = 0;
	public List<Quest> quests;

	public QuestDatabase() {
		quests = new List<Quest>();		
	}

	void Awake(){
		Resources.LoadAll("Quests");
		foreach(Quest quest in Resources.FindObjectsOfTypeAll<Quest>()){
			quest.ID = ++globalID;
			quest.isAccepted = false;
			quest.isCompleted = false;
			quests.Add(quest);
		}
	}


	public List<Quest> getAcceptedQuests(){
		return quests.FindAll( quest => quest.isAccepted && !quest.isCompleted );
	}

	public List<Quest> getCompletedQuests(){
		return quests.FindAll( quest => quest.isCompleted );
	}

	public Quest findQuestById(int id) {
		return quests.FindLast( quest => quest.ID == id );
	}

}











