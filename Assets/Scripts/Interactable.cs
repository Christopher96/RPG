﻿using UnityEngine;

public class Interactable : MonoBehaviour {

    protected bool playerIsPresent = false;

	public Collider2D interactableCol;

    private Player player;

    void Start() {

        if (interactableCol == null) interactableCol = GetComponent<Collider2D>();
        player = FindObjectOfType<Player>();
    }

    public void Update() {
        playerCheck();
    }

    public void playerCheck() {
		if (interactableCol != null && M.player.groundCol != null)
            playerIsPresent = interactableCol.IsTouching(M.player.groundCol);
        else
            playerIsPresent = false;
    }

}