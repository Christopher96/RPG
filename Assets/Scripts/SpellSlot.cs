﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpellSlot : MonoBehaviour {

	public Image icon, background;
	public Text title;
	public PlayerSpell spell;

	void Start(){
		background = GetComponent<Image> ();
        GetComponent<Toggle>().group = S.spell.toggleGroup;
	}
		
	public void updateSlot(){
		if( spell != null ){
			icon.sprite = spell.sprite;
			title.text = spell.name;

			switch( spell.GetType().Name ){
				case "MagicSpell":
					background.color = D.spell.magicSpellColor; break;
				case "RangeSpell":
					background.color = D.spell.rangeSpellColor; break;
				case "MeleeSpell":
					background.color = D.spell.meleeSpellColor; break;
			}
		}
	}
}
