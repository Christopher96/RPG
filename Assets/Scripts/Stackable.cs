﻿using UnityEngine;
using System.Collections;

public class Stackable : MonoBehaviour {
	public int maxStack = 5;
	public int amount = 1;

	public int freeSlots {
		get {
			return maxStack - amount;
		}
	}

	void Awake() {
		if(amount < 1) amount = 1;
		if(amount > maxStack) amount = maxStack;
	}

	public int increase(int value) {
		amount += value;
		int rest = 0;

		if(amount > maxStack){
			rest =  amount - maxStack;
			amount = maxStack;
		} 

		return rest;
	}

	public void decrease(int value) {
		amount -= value;
		
		if(amount < 1){
			amount = 1;
		}
	}
}
