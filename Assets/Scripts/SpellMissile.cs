﻿using UnityEngine;
using System.Collections.Generic;

public class SpellMissile : MonoBehaviour {

    Color32 color1;
    Color32 color2;
    ParticleSystem spellSystem;

    public List<MissileColor> missileColors;

    void Start() {

        spellSystem = GetComponent<ParticleSystem>();
    }

    void FixedUpdate() {
        Color color = Color.Lerp( color1, color2, Mathf.PingPong( Time.time, 1 ) );
        spellSystem.startColor = color;
    }

    public void loadSpell( MagicSpell spell ) {
        this.gameObject.SetActive( true );
        
        MissileColor colorObject = missileColors.Find(color => color.type == spell.type);

        if( colorObject != null ){
            color1 = colorObject.color1;
            color2 = colorObject.color2;
        } else {
            color1 = Color.cyan;
            color2 = Color.blue;
        }
    }
}

[System.Serializable]
public class MissileColor {
    public MagicSpellType type;
    public Color32 color1;
    public Color32 color2;
}
