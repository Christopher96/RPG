using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System;

public class Functions : MonoBehaviour {
	public static bool hasScript( string scriptName, GameObject gameObj ){
		if( gameObj.GetComponent( scriptName ) != null ){
			return true;
		}

		return false;
	}
		
	public static void flipGameobject(Transform target, bool facingRight) {
		Vector3 scale = target.localScale;
        if((facingRight && scale.x > 0) || (!facingRight && scale.x < 0))
		    scale.x *= -1;
		target.localScale = scale;
	}

    public static IEnumerator wait( float delay, Action callback ) {
        yield return new WaitForSeconds( delay );
        callback();
    }

    public static void click( GameObject gameObject ) {
        var pointer = new PointerEventData( EventSystem.current );
        ExecuteEvents.Execute( gameObject, pointer, ExecuteEvents.pointerClickHandler );
    }

    public static void click( Transform transform ) {
        click( transform.gameObject );
    }

	public static string colorToHex(Color32 color) {
		string hex = "#" + color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
		return hex;
	}

	public static Color ConvertColor (Color color){
		return new Color(color.r*255, color.g*255, color.b*255);
	}
}


public static class ToggleGroupExtension {
	public static Toggle GetActive(this ToggleGroup group) {
		return group.ActiveToggles().FirstOrDefault();
	}
}