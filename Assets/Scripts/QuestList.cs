﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestList : MonoBehaviour {

	public Toggle newQuestsTab;
	public Transform questListContent, questSlotPrefab;
	public GameObject noNewQuests, noCompletedQuests;

	void Start() {

		newQuestsTab.onValueChanged.AddListener(delegate{
			getQuestList();
		});

		getQuestList();
	}


	public void getQuestList(){

		QuestTab tab = (newQuestsTab.isOn) ? QuestTab.Accepted : QuestTab.Completed;
		List<Quest> questList = (tab == QuestTab.Accepted) ? D.quest.getAcceptedQuests() : D.quest.getCompletedQuests();
		
		foreach( Transform child in questListContent ) {
			Destroy( child.gameObject );
		}

		bool noQuests = true;

		if( questList.Count > 0 ){
			noQuests = false;

			foreach( Quest quest in questList ){

				Transform questContainer = Instantiate( questSlotPrefab ) as Transform;

				QuestSlot questSlot = questContainer.GetComponent<QuestSlot>();
				questSlot.quest = quest;

				questContainer.SetParent( questListContent );
				questContainer.transform.localScale = Vector3.one;

				if(quest == questList[questList.Count - 1]){
					questSlot.toggle.isOn = true;
				}
			}

			
		}

		noNewQuests.SetActive( tab == QuestTab.Accepted && noQuests );
		noCompletedQuests.SetActive( tab == QuestTab.Completed && noQuests );
	}
}


public enum QuestTab{
	Accepted, Completed
}