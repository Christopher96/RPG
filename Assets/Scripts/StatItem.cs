﻿using System.Collections.Generic;

[System.Serializable]
public class StatItem : Item {
	public List<ItemCombatStat> stats;

	public ItemCombatStat getStat(CombatStatType type){
		return stats.Find(stat => stat.type == type);
	}
}
