﻿using System.Collections.Generic;

[System.Serializable]
public class Dialog {
    public string text;
    public bool isPlayerDialog;
    public bool isInstant;
    public PositiveDialogAction positiveAction;
	public NegativeDialogAction negativeAction;
    public string positiveText;
    public string negativeText;
}

public enum PositiveDialogAction {
    NextDialog, QuestAccept, QuestComplete, QuestCheck, QuestDialog, Shop, Ok
}

public enum NegativeDialogAction {
    PrevDialog, Shop, Decline, Cancel, No, Leave
}

