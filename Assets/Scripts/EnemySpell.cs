﻿using UnityEngine;
using System.Collections.Generic;

public class EnemySpell : Spell {
	public EnemySpellType type;

	public List<EnemyCurse> curses;
	public List<EnemyBuff> buffs;

	void Start() {
		if(curses == null) curses = new List<EnemyCurse>();
		if(buffs == null) buffs = new List<EnemyBuff>();
	}
}


[System.Serializable]
public class EnemySpellStat {
	public PlayerStatType type;
	public int value;
}


public enum EnemySpellType {
	Melee, Magic, Range
}
