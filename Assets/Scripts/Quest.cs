﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Quest : MonoBehaviour {
	[System.NonSerialized]
	public int ID = 0;
	public bool isCompleted;
	public bool isAccepted;

	public string description;

	public int xpReward;
	public int goldReward;
	public List<QuestItemReward> itemRewards;

	public List<QuestEnemyRequirement> enemyRequirements;
	public List<QuestItemRequirement> itemRequirements;

	public Sprite sprite {
		get {
			return GetComponent<SpriteRenderer>().sprite;
		}
	}
}

[System.Serializable]
public class QuestItemReward {
	public Item item;
    public int amount = 1;
}

[System.Serializable]
public class QuestItemRequirement {
	public Item item;
    public int amount;
}


[System.Serializable]
public class QuestEnemyRequirement {
    public Enemy enemy;
    public int amount;

	[System.NonSerialized]
    public int current;
}
