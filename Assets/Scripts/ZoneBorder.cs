﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneBorder : MonoBehaviour {
	public ZoneBorderType type;
	public BoxCollider2D col;

	void Awake() {
		col = GetComponent<BoxCollider2D>();
	}
}

public enum ZoneBorderType {
	Left, Right
}
