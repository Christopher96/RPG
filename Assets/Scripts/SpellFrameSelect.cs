﻿public class SpellFrameSelect : FrameSelect {

	new void Update () {
		if(S.spell.selected){
			PlayerSpell spell = S.spell.selectedspell;

			title.text = spell.name;
			description.text = spell.description;
			icon.sprite = spell.sprite;
		}
	}
}
