﻿using UnityEngine;
using System.Collections;

public class FrontBackHandeler : MonoBehaviour {

	public BoxCollider2D groundCol;
	public SpriteRenderer rend;

	public float yGround;

	void Update () {
		frontBackCheck ();
	}

	public void frontBackCheck () {
		if( groundCol != null )
			yGround = groundCol.bounds.center.y;
		rend.sortingLayerName = (M.player.yGround > yGround) ?  "Front" : "Back";
	}
}
