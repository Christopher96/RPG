﻿using UnityEngine;
using UnityEngine.UI;

public class CombatUI : MonoBehaviour {

	public CombatFrame playerFrame, enemyFrame;
    public Text enemyDefence, enemyAttack;
    public SpellAlert spellAlert;

    void Update() {
         GetComponent<Animator>().SetBool( "showUI", M.combat.showUI );
    }

    public void leaveCombat() {
        M.combat.isFinished = true;
        if(M.player.state != PlayerState.Escaping)
            M.combat.inCombat = false;

        M.actionbar.UI.anim.enabled = false;

        if(!M.player.isDead && M.combat.enemy.isDead) {
            M.experience.addXP(M.combat.enemy.experience, true);
        } else {
            M.UI.state = UIStates.Default;
        }
    }
}
