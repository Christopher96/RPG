﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour {

	public Text goldText;
	public int totalGold = 0;

	public Transform _inventorySlots;

	public bool doAddToInventory;
	public List<Item> addToInventoryList;
	public List<InventorySlot> inventorySlots;

	public int maxSlots;
	public Alert alert;

	void Awake() {
		inventorySlots = new List<InventorySlot>(_inventorySlots.GetComponentsInChildren<InventorySlot>());
		maxSlots = inventorySlots.Count;

		if (doAddToInventory) {
			addToInventory();
		}
	}

	void Update() {
		goldText.text = totalGold.ToString();
	}

	public void addToInventory() {
		foreach (Item item in addToInventoryList) {
			addItem(item, false);
		}
	}

	public void toggleInventory() {
		if (M.UI.state == UIStates.Inventory)
			M.UI.state = UIStates.Default;
		else M.UI.state = UIStates.Inventory;
	}

	public int getItemRequirement(QuestItemRequirement requirement) {

		int amount = getAmountByItem(requirement.item);
		if (amount > requirement.amount) {
			return requirement.amount;
		} else {
			return amount;
		}
	}

	public bool isFull {
		get {
			return freeSlots < 1;
		}
	}

	public int freeSlots {
		get {
			return emptySlots.Count;
		}
	}

	public List<InventorySlot> emptySlots {
		get {
			return inventorySlots.FindAll(slot => slot.item == null);
		}
	}

	public bool hasUniqueItem(Item item) {
		return slotByUniqueItem(item) != null;
	}

	public InventorySlot slotByUniqueItem(Item item) {
		return inventorySlots.Find(slot => slot.item == item);
	}

	public InventorySlot slotByItem(Item item) {
		return inventorySlots.Find(slot => slot.item != null && slot.item.ID == item.ID);
	}

	public List<InventorySlot> slotsByItem(Item item) {
		return inventorySlots.FindAll(slot => slot.item != null && slot.item.ID == item.ID);
	}

	public List<Stackable> stackablesByItem(Item item) {
		List<Stackable> stackables = new List<Stackable>();
		slotsByItem(item).FindAll(slot => slot.item.has<Stackable>()).ForEach(slot => stackables.Add(slot.item.get<Stackable>()));
		return stackables;
	}

	public int freeStackableSlots(Item item, int maxStack) {
		int freeSlots = 0;
		stackablesByItem(item).ForEach(stackable => freeSlots += stackable.freeSlots);
		freeSlots += (emptySlots.Count * maxStack);
		return freeSlots;
	}

    public void emptyInventory() {
        foreach(InventorySlot slot in inventorySlots) {
			if(slot.item != null) {
				slot.item.Delete();
			}
        }
    }

	public bool addItem(Item item, int amount) {
		for (int i = 1; i <= amount; i++) {
			bool select = (i == amount);
			if (!addItem(item, select))
				return false;
		}

		return true;
	}

	public bool addItem(Item item, bool select = false) {

		item = item.Clone();

		if (item.has<Stackable>()) {
			Stackable stackable = item.get<Stackable>();

			int freeStackableSlots = 0;

			freeStackableSlots += freeSlots * stackable.maxStack;

			foreach (Stackable _stackable in stackablesByItem(item)) {
				freeStackableSlots += _stackable.freeSlots;
			}

			int rest = 0;
			if (freeStackableSlots > stackable.amount) {
				foreach (Stackable _stackable in stackablesByItem(item)) {
					rest = _stackable.increase(stackable.amount);
					stackable.amount = rest;
					if (rest == 0) return true;
				}
			}
		}

		if (freeSlots > 0) {

			InventorySlot slot = emptySlots[0];
			slot.item = item;

			if (select)
				slot.toggle.isOn = true;

			return true;
		} else {
			alert.prompt(
				delegate {

				},
				delegate {
					M.UI.state = UIStates.Default;
				}, "Inventory is full",
				"Open", "Cancel"
			);

			item.Remove();
			return false;
		}
	}

	public void removeItem(Item item, int amount = 1, bool drop = false) {
		for (int i = 0; i < amount; i++) {
			ItemSlot slot = slotByItem(item);
			if (drop) slot.item.Drop();
			else slot.item.Remove();
		}
	}

	public void removeUniqueItem(Item item) {
		item.Remove();
	}

	public int getAmountByItem(Item item) {

		int total = 0;

		foreach (InventorySlot slot in slotsByItem(item)) {
			if (slot.item.has<Stackable>()) {
				total += slot.item.get<Stackable>().amount;
			} else {
				total += 1;
			}
		}

		return total;
	}

	public InventorySlot getSlotByIndex(int index) {
		return inventorySlots[index];
	}

}
