﻿using UnityEngine;
using UnityEngine.UI;

public class QuestSelect : MonoBehaviour {
	
    public bool selected = false;
    public ToggleGroup toggleGroup;
	public QuestSlot selectedSlot;
    public Quest selectedQuest;

    public GameObject questInfo, noQuest;


    void Awake() {
        toggleGroup = GetComponent<ToggleGroup>();
    }


	void Update(){

		Toggle toggle = toggleGroup.GetActive();
		if(toggle != null && toggle.isOn){
			selectedSlot = toggleGroup.GetActive().GetComponent<QuestSlot>();
			selectedQuest = selectedSlot.quest;
		} else {
			selectedQuest = null;
		}

		selected = (selectedQuest != null);

		if( !selected ) {
			noQuest.GetComponent<Text>().text = M.quest.getCompletedQuestsText();
		}

        questInfo.SetActive( selected );
        noQuest.SetActive( !selected );
    }
}
