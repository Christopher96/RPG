﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHeadSprite : MonoBehaviour {
	
	Image headImage;

	void Awake() {
		headImage = GetComponent<Image>();
	}

	void Update() {
		EquipmentRend equipmentRend = M.sprite.getEquipmentRend(EquipmentType.Head);

		if(equipmentRend != null) {
			if(equipmentRend.rend.enabled) {
				headImage.enabled = true;
				headImage.sprite = equipmentRend.rend.sprite;
			} else {
				headImage.enabled = false;
			}
 		}
	}
}
