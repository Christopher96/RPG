﻿using System.Collections.Generic;
using UnityEngine;

public class LootChest : InteractableButton {

    public List<Item> table;
    public Sprite openedSprite, closedSprite;
    SpriteRenderer rend;

	void Start(){
		interactText = "Tap to loot";
        rend = GetComponent<SpriteRenderer>();
        M.UI.lootClose.AddListener(closeChest);
        M.loot.changeLootChest(this);
    }

    public void updateSprite() {
        if(table.Count > 0) {
            rend.sprite = closedSprite;
        } else {
            rend.sprite = openedSprite;
        }
    }

	public override void interact (){
		M.UI.state = UIStates.Loot;
        rend.sprite = openedSprite;
    }

    public void closeChest() {
        updateSprite();
    }
}
