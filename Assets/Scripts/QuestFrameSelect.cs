﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestFrameSelect : FrameSelect {

	public Text requirementText;

	void Update () {
		if(S.quest.selected){
			Quest quest = S.quest.selectedQuest;

			title.text = quest.name;
			icon.sprite = quest.sprite;
			requirementText.text = M.quest.getRequirementsText( quest );
		}
	}
}
