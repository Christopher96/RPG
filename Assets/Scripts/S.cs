﻿using UnityEngine;
using System.Collections;

public class S : MonoBehaviour {

	public static ItemSelect item;
	public static QuestSelect quest;
	public static LootSelect loot;
	public static SpellSelect spell;

	public ItemSelect _item;
	public QuestSelect _quest;
	public LootSelect _loot;
	public SpellSelect _spell;

	void Awake() {
		item = _item;
		quest = _quest;
		loot = _loot;
		spell = _spell;
	}
}
