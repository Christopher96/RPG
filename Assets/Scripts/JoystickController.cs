﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class JoystickController : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {
	public static bool isLefty;
	public float paddingPx;

	public Image joystickDisc;
	public Image joystickPad;
	public Vector3 inputVector;

	void Update() {
		RectTransform rect = joystickDisc.rectTransform;
		Vector2 anchorMax = Vector2.zero;
		Vector2 anchorMin = Vector2.zero;
		if(!isLefty) {
			anchorMax.x = 1;
			anchorMin.x = 1;
		}
		rect.anchorMax = anchorMax;
		rect.anchorMin = anchorMin;
		Vector2 offsetMax = Vector2.zero;
		Vector2 offsetMin = Vector2.zero;
		offsetMin.x = (isLefty) ? paddingPx : -paddingPx;
		offsetMin.y = paddingPx;
		rect.offsetMin = offsetMin;
		rect.sizeDelta = Vector2.one;
	}

	public virtual void OnDrag(PointerEventData ped){
		Vector2 pos;
		if(RectTransformUtility.ScreenPointToLocalPointInRectangle (joystickDisc.rectTransform, ped.position, ped.pressEventCamera, out pos)){
			pos.x = (pos.x / joystickDisc.rectTransform.sizeDelta.x);
			pos.y = (pos.y / joystickDisc.rectTransform.sizeDelta.y);

			inputVector = new Vector3 (pos.x * 2, pos.y * 2, 0);
			inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

			joystickPad.rectTransform.anchoredPosition = new Vector3 (
				inputVector.x * (joystickDisc.rectTransform.sizeDelta.x / 2),
				inputVector.y * (joystickDisc.rectTransform.sizeDelta.y / 2)
			);
		}
	}

	public virtual void OnPointerDown(PointerEventData ped){ 
		OnDrag (ped);
	}

	public virtual void OnPointerUp(PointerEventData ped){
		Reset();
	}

	void OnEnable() {
		Reset();
	}

	void OnDisable() {
		Reset();
	}

	void Reset(){
		inputVector = Vector3.zero;
		joystickPad.rectTransform.anchoredPosition = Vector3.zero;
	}

	public float Horizontal(){
		return inputVector.x;
	}

	public float Vertical(){
		return inputVector.y;
	}


}
