﻿using UnityEngine.UI;

public class InventorySlot : ItemSlot {

    public Text stackText;

    new void Start() {
        base.Start();
        toggle.group = S.item.toggleGroup;
        toggle.onValueChanged.AddListener(delegate {
            S.item.selectItem(item);
        });
        stackText = itemIcon.GetComponentInChildren<Text>();
    }

    void Update(){
        if(item != null){
            itemIcon.enabled = true;
            itemIcon.sprite = item.sprite;

            if (item.has<Stackable>()) {
                stackText.text = (item.get<Stackable>()).amount.ToString();
                stackText.enabled = true;
            } else {
                stackText.enabled = false;
            }

        } else {
            itemIcon.enabled = false;
            stackText.enabled = false;
        }
    }
}
