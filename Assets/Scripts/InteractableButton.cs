﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableButton : Interactable {

	public string interactText;
	public virtual void interact(){}

	float originalWidth = 800;  // define here the original resolution
	float originalHeight = 480; // you used to create the GUI contents 
	private Vector3 scale;

	void OnGUI(){
		if ( playerIsPresent && M.UI.state == UIStates.Default && !M.zone.isLoading ) {
			scale.x = Screen.width/originalWidth; // calculate hor scale
			scale.y = Screen.height/originalHeight; // calculate vert scale
			scale.z = 1;
			Matrix4x4 svMat = GUI.matrix; // save current matrix
			// substitute matrix - only scale is altered from standard
			GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, scale);
			// draw your GUI controls here:

			Rect rect = new Rect( 175, 215, 450, 50 );
			GUIStyle style = GUI.skin.button;
			style.alignment = TextAnchor.MiddleCenter;
			style.fontSize = 20;
			style.font = UIManager._globalFont;

			if( GUI.Button( rect, interactText, style ) ) {
				interact();
			}
			//...
			// restore matrix before returning
			GUI.matrix = svMat; // restore matrix
		}
	}
}
