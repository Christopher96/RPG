﻿public class ItemFrameSelect : ButtonFrameSelect {
    public void updateSelect(Item item){
        title.text = item.name;
        description.text = item.description;
        icon.sprite = item.sprite;

        title.color = D.item.getRarityColor(item.rarity);

        positiveBtnTxt = S.item.positiveBtnTxt;
        negativeBtnTxt = S.item.negativeBtnTxt;
    }
}
