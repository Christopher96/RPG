﻿using UnityEngine;
using System.Collections.Generic;

public class StatBonusMananger : MonoBehaviour {

	CombatStatType bonusType;
	List<ItemCombatStat> bonusStats;

	void Update() {

		bonusStats = new List<ItemCombatStat>();

		foreach(CombatStat combatStat in M.stat.combatStats){
			int value = 0;
			bonusType = combatStat.type;

			if(S.item.selected){

				Item selectedItem = S.item.selectedItem;

				if (selectedItem is StatItem) {

					StatItem selectedStatItem = selectedItem as StatItem;
					ItemCombatStat selectedStat = selectedStatItem.getStat(bonusType);
					ItemCombatStat equipedStat = null;
					ItemCombatStat secondStat = null;

					if(selectedStatItem is EquipmentItem){

						EquipmentItem selectedEquipmentItem = selectedStatItem as EquipmentItem;

						secondStat = checkForTwoHand(selectedEquipmentItem, bonusType);

						if(M.equipment.isEquiped(selectedEquipmentItem.equipmentType)){
							equipedStat = M.equipment.getEquipmentSlotItem(selectedEquipmentItem.equipmentType).getStat(combatStat.type);
						}
					}

					if(selectedStat != null){
						value += selectedStat.value;
					}

					if(equipedStat != null){
						value -= equipedStat.value;
					}

					if(secondStat != null){
						value -= secondStat.value;
					}				
				}
			}

			addBonus(value);
		}

		loadBonuses();
	}


	void addBonus(int value){
		bonusStats.Add(new ItemCombatStat(bonusType, value));
	}

	ItemCombatStat checkForTwoHand(EquipmentItem selectedEquipmentItem, CombatStatType type){
		if(selectedEquipmentItem is Weapon){
			Weapon weapon = selectedEquipmentItem as Weapon;

			if(weapon.isTwoHand){
				if(M.equipment.isEquiped(EquipmentType.Offhand)){
					return M.equipment.getEquipmentSlotItem(EquipmentType.Offhand).getStat(type);
				}
			}
		} else if( selectedEquipmentItem.equipmentType == EquipmentType.Offhand ){

			if(M.equipment.twoHandIsEquiped()){
				return M.equipment.getEquipmentSlotItem(EquipmentType.Mainhand).getStat(type);
			}
		}

		return null;
	}

	void loadBonuses() {
		foreach (ItemCombatStat stat in bonusStats) {
			M.stat.getStat(stat.type).visualBonus = stat.value;
		}
	}



}



