﻿using UnityEngine;
using UnityEngine.UI;

public class CombatStatText : MonoBehaviour {

	public CombatStatType type;
    public Text statText;

    void Awake() {
        statText = GetComponent<Text>();
    }
    
    void Update() {
        statText.text = getStatText();
    }

    public string getStatText() {
        CombatStat stat = M.stat.getStat(type);

        int current = stat.currentValue;
        int total = stat.totalValue;

        string text = "";

        if( current != total ) {
            string color = (current > total) ? StatManager.buffedColor : StatManager.debuffedColor;
            text += "<color=" + color + ">" + current.ToString() + "</color>";
        } else {
            text += current.ToString();
        }

        bool hitPointCheck = stat.type == CombatStatType.Hitpoints && stat.visualBonus != 0;
        
        if(!hitPointCheck) {
            text += "/" + total.ToString();
        }

        return text;
    }
}
