﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	
	public bool isFlipped = false;

	public float horizontalSpeed = 1f;
	public float verticalSpeed = 0.5f;

	public JoystickController joystickController;

	public Collider2D contactCol;
	
	private Rigidbody2D rb;

	void Start(){
		rb = GetComponent <Rigidbody2D> ();
	}

	public float startX;
	public float attackRange = 0.03f;

	// Update is called once per frame
	void FixedUpdate () {

		if( M.player.state != PlayerState.Dead ) {
			if( M.combat.inCombat ){
				
				bool enemycheck = M.player.contactCol.IsTouching( M.combat.enemy.contactCol );

				switch (M.player.state) {
					case PlayerState.Entering:
						moveToCenter();
					break;
					case PlayerState.Escaping:
						moveLeft(1);
					break;
					case PlayerState.Charging:
						if( enemycheck ) {
							M.player.attack();
						} else {
							moveRight();
						}
					break;
					case PlayerState.Retreating:
						if( transform.position.x <= startX ) {
                            Vector2 zero = Vector2.zero;
                            rb.velocity = zero;

                            isFlipped = false;
							M.player.switchTurn();
						} else {
							moveLeft();
						}
					break;
					default:
					break;
				}

			} else {
				move ( joystickController.Horizontal (), joystickController.Vertical () ); 
			}
		}
    }

	float stopVelocity = 0.15f;

	void Update() {
		checkFlip();

		if(!M.combat.inCombat) {

			if(( Mathf.Abs (rb.velocity.x) > stopVelocity || Mathf.Abs (rb.velocity.y) > stopVelocity ) ||
			   ( hForce > 0 || vForce > 0 )) {
				M.player.state = PlayerState.Running;
			} else {
				M.player.state = PlayerState.Idle;
			}
		}
	}

	public void freeze(bool doFreeze){
		rb.constraints = (doFreeze) ? RigidbodyConstraints2D.FreezeAll : RigidbodyConstraints2D.FreezeRotation;
	}

	public void moveRight(float speed=1f) {
		move (speed, 0);
	}

	public void moveLeft(float speed=1f){
		move (-speed, 0);
	}

	float hForce = 0;
	float vForce = 0;

	private void move( float horizontal, float vertical ){
		freeze(false);

		if( horizontal > 0 ){
			isFlipped = false;
		} else if (horizontal < 0) {
			isFlipped = true;
		}

		hForce = horizontal * horizontalSpeed;
		vForce = vertical * verticalSpeed;
		
		rb.AddForce ( new Vector2( hForce, vForce ) );
	}

	Vector3 targetPos;

	public void startEntering() {
		targetPos = transform.position;
		
		Enemy enemy = M.combat.enemy;

		if(enemy != null) {
        	targetPos.y = enemy.yGround;
		} else {
			targetPos.y = M.zone.Yoffset;
		}

		M.player.state = PlayerState.Entering;
	}

	private void moveToCenter(){
		float ySpeed = 1f;
		
		if(Mathf.Abs(M.player.yGround - targetPos.y) < 0.01f ) {
			ySpeed = 0;
		} else if(M.player.yGround > targetPos.y) {
			ySpeed *= -1f;
		}

		move(1f, ySpeed);
	}

    public void checkFlip() {
        Vector3 scale = transform.localScale;

        if( ( scale.x > 0 && isFlipped ) || ( scale.x < 0 && !isFlipped ) ){
            scale.x *= -1;
            transform.localScale = scale;
        }
        
    }
}
