﻿using UnityEngine;

[System.Serializable]
public class Stat : MonoBehaviour {
	public Sprite icon {
		get {
			return GetComponent<SpriteRenderer>().sprite;
		}
	}
	public int startValue;
	public int currentValue;
}