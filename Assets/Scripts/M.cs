﻿using System.Collections;
using UnityEngine;

public class M : MonoBehaviour {

	public static GameUpdate game;
	public static StatManager stat;
	public static EquipmentManager equipment;
	public static EquipmentSpriteManager sprite;
	public static InventoryManager inventory;
	public static ExperienceManager experience;
	public static QuestManager quest;
	public static ActionbarManager actionbar;
	public static CameraManager camera;
	public static SpellManager spell;
	public static ZoneManager zone;
	public static LootManager loot;
	public static CombatManager combat;
	public static DialogManager dialog;
	public static ShopManager shop;
	public static Player player;
	public static PlayerMovement movement;
	public static UIManager UI;
	public static SettingsManager settings;

	public GameUpdate _game;
	public StatManager _stat;
	public EquipmentManager _equipment;
	public EquipmentSpriteManager _sprite;
	public InventoryManager _inventory;
	public ExperienceManager _experience;
	public QuestManager _quest;
	public ActionbarManager _actionbar;
	public CameraManager _camera;
	public SpellManager _spell;
	public ZoneManager _zone;
	public LootManager _loot;
	public CombatManager _combat;
	public DialogManager _dialog;
	public ShopManager _shop;
	public Player _player;
	public PlayerMovement _movement;
	public UIManager _UI;
	public SettingsManager _settings;

	void Awake() {
		game = _game;
		stat = _stat;
		equipment = _equipment;
		sprite = _sprite;
		inventory = _inventory;
		experience = _experience;
		quest = _quest;
		actionbar = _actionbar;
		camera = _camera;
		spell = _spell;
		zone = _zone;
		loot = _loot;
		combat = _combat;
		dialog = _dialog;
		shop = _shop;
		player = _player;
		movement = _movement;
		UI = _UI;
		settings = _settings;
	}
}