﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSwapper : MonoBehaviour {

	SpriteRenderer mSpriteRenderer;
	Texture2D mColorSwapTex;
	Color[] mSpriteColors;

	public void Start() {
		mSpriteRenderer = GetComponent<SpriteRenderer>();
		initColorSwapTex();
	}

	public void initColorSwapTex() {
		Texture2D colorSwapTex = new Texture2D(256, 1, TextureFormat.RGBA32, false, false);
		colorSwapTex.filterMode = FilterMode.Point;
	
		for (int i = 0; i < colorSwapTex.width; ++i)
			colorSwapTex.SetPixel(i, 0, new Color(0.0f, 0.0f, 0.0f, 0.0f));
	
		colorSwapTex.Apply();
	
		mSpriteRenderer.material.SetTexture("_SwapTex", colorSwapTex);

		mSpriteColors = new Color[colorSwapTex.width];
		mColorSwapTex = colorSwapTex;
	}

	public void swapColor(ColorSwap colorSwap) {
		foreach(Color color in colorSwap.mainColors) {
			Color convertedColor = Functions.ConvertColor(color);
			mColorSwapTex.SetPixel((int)convertedColor.r, 0, colorSwap.swapColor);
		}

		mColorSwapTex.Apply();
	}
}

[System.Serializable]
public class ColorSwap {
	public List<Color> mainColors;
	public Color swapColor;
}