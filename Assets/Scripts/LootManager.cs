﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootManager : MonoBehaviour {

    public string currentZone;
    public LootChest currentChest;
    public List<ChestSave> chestSaves;

    public float lootSpawnDelay = 0.1f;
    public GameObject groundLootPrefab;
    public List<Loot> lootshizzle;
    public LootUI lootUI;

    public LootManager() {
        chestSaves = new List<ChestSave>();
    }

    void Update() {
        if (Input.GetKeyDown("o")) spawnLootTable(M.player.transform.position.x, lootshizzle);
    }

    public void spawnLootTable(float xPos, List<Loot> lootTable) {
        StartCoroutine(iterateLootTable(xPos, lootTable));
    }

    IEnumerator iterateLootTable(float xPos, List<Loot> lootTable) {
        foreach (Loot loot in lootTable) {
            float random = Random.Range(0, 100);
            if (random <= loot.dropChance) {
                spawnItem(xPos, loot.item);
                yield return new WaitForSeconds(lootSpawnDelay);
            }
        }
    }

    public void spawnItem(float xPos, Item item) {
        GameObject groundLootCopy = Instantiate(groundLootPrefab,
            new Vector2(xPos, groundLootPrefab.transform.position.y),
            Quaternion.identity) as GameObject;
        GroundLoot groundLoot = groundLootCopy.GetComponent<GroundLoot>();

        Item clone = item.Clone();
        if (clone.has<Stackable>()) {
            clone.get<Stackable>().amount = 1;
        }

        groundLoot.item = clone;
    }

    public void wipeGroundLoot() {
        GroundLoot[] loots = FindObjectsOfType<GroundLoot>();
        foreach (GroundLoot loot in loots) {
            loot.item.Remove();
            Destroy(loot.gameObject);
        }
    }

    public void changeLootChest(LootChest chest) {
        currentChest = chest;
        currentZone = M.zone.currentZone;
        ChestSave chestSave = findChestSave();
        if (chestSave != null) {
            List<Item> newTable = new List<Item>();
            foreach (int itemID in chestSave.itemsLeft) {
                Item item = D.item.getItemById(itemID);
                newTable.Add(item);
            }
            currentChest.table = newTable;
            currentChest.updateSprite();
        }
        lootUI.lootList.updateLootList();
    }

    public void lootTable(Item item) {
        if( M.inventory.addItem(item) ){
            if( currentChest.table.Count > 0 ) {
                currentChest.table.Remove( item );
                lootUI.lootList.updateLootList();
                saveChest();
            }
        }
    }

    public void saveChest() {
        List<int> itemsLeft = new List<int>();
        foreach (Item item in currentChest.table) {
            itemsLeft.Add(item.ID);
        }

        ChestSave chestSave = findChestSave();
        if (chestSave == null) {
            chestSaves.Add(new ChestSave(currentZone, itemsLeft));
        } else {
            chestSave.itemsLeft = itemsLeft;
        }
    }

    public ChestSave findChestSave() {
        return chestSaves.Find(save => save.zone.Equals(currentZone));
    }
}

[System.Serializable]
public class Loot {
    [Range(0, 100)]
    public int dropChance = 100;
    public Item item;
}

[System.Serializable]
public class ChestSave {
    public string zone;
    public List<int> itemsLeft;

    public ChestSave(string _zone, List<int> _itemsLeft) {
        zone = _zone;
        itemsLeft = _itemsLeft;
    }
}
