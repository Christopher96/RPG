﻿using UnityEngine;
using System.Collections;

public class Weapon : EquipmentItem {

	public WeaponRendType rendType;
	public bool isTwoHand = false;

	void Awake() {
		equipmentType = EquipmentType.Mainhand;
		// if(isTwoHand){
		// 	this.name = this.name + " (2h)";
		// }
	}

}
