﻿using UnityEngine;
using UnityEngine.UI;

public class GroundLoot : MonoBehaviour {

    public Item item;
    
    float awakeTime;

    public float amplitude = 0.1f;
    public float period = 2f;
    public float attractSpeed = 0.5f;

    public bool playerIsPresent = false;

    SpriteRenderer rend;
    BoxCollider2D col;
    Player player;

    void Awake() {
        rend = GetComponent<SpriteRenderer>();
        col = GetComponent<BoxCollider2D>();
        player = FindObjectOfType<Player>();

        awakeTime = Time.time;

        calcTargetPos();
    }

    Vector2 targetBumpPos;

    void calcTargetPos() {
        targetBumpPos = new Vector2( transform.position.x + Random.Range( -0.15f, 0.15f ), transform.position.y + Random.Range( -0.1f, 0.1f ) );
    }

    void bump() {
        float step = 2f * Time.deltaTime;
        transform.position = Vector2.Lerp( transform.position, targetBumpPos, step );
    }

    Vector2 startPos;
    bool waveStarted = false;
    float waveStartTime;

    void wave() {
        Vector2 pos = transform.position;
        pos.y = startPos.y + amplitude * Mathf.Sin( period * ( Time.time - waveStartTime) );
        transform.position = pos;
    }

    void loot() {
        float step = attractSpeed * Time.deltaTime;
        transform.position = Vector2.Lerp( transform.position, player.transform.position, step);
    }

    bool tapped = false;

    void Update () {
        if( item != null ) {
            lootUpdate();
        }        
    }

    void lootUpdate() {
        if( !tapped ) {
            if (Time.time - awakeTime > 1f) {
                if( !waveStarted ) {
                    startPos = transform.position;
                    waveStartTime = Time.time;
                    waveStarted = true;
                }

                wave();
            } else bump();
        } else {
            loot();
        }

		if ( Input.GetMouseButtonDown(0) && M.UI.state == UIStates.Default ){

			Vector3 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			Vector2 pos2D = new Vector2( pos.x, pos.y );

            Collider2D collider = Physics2D.OverlapCircle(pos2D, 0.05f, 1 << LayerMask.NameToLayer("GroundLoot") );

			if ( collider != null ) {
				if (collider == this.col && !tapped ) {
					tapped = true;
				}
			}
		}

        rend.sprite = item.sprite;
    }

    void OnTriggerStay2D( Collider2D targetCol ) {
        if ( targetCol == player.pickupCol && tapped ) {
            tapped = false;
            if(M.inventory.addItem(item)) {
                item.Remove();
                Destroy(this.gameObject);
            }
        }
    }
}
