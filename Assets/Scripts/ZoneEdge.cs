﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneEdge : MonoBehaviour {
	public ZoneEdgeType type;
	public BoxCollider2D col;

	void Awake() {
		col = GetComponent<BoxCollider2D>();
	}
}

public enum ZoneEdgeType {
	Left, Right
}