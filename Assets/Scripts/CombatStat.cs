﻿
public enum CombatStatType {
	Defence, Strength, Range, Magic, Hitpoints
}

[System.Serializable]
public class CombatStat : Stat {
	public CombatStatType type;
	public int totalValue;
	public int visualBonus;
}