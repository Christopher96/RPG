﻿
public enum CommonStatType {
	Gold, Experience
}

[System.Serializable]
public class CommonStat : Stat {
	public CommonStatType type;
}