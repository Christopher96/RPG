﻿using UnityEngine;
using UnityEngine.UI;

public class FrameManager : MonoBehaviour
{

    public GameObject questFrameSelect, itemFrameSelect, spellFrameSelect, nothingText;
    public Button prevStateBtn, nextStateBtn;

    void Update()
    {

        prevStateBtn.interactable = nextStateBtn.interactable = (M.UI.state != UIStates.Shop);

        switch (M.UI.state)
        {
            case UIStates.Spells:
                spellFrameSelect.SetActive(S.spell.selected);

                nothingText.SetActive(!S.spell.selected);

                itemFrameSelect.SetActive(false);
                questFrameSelect.SetActive(false);
                break;
            case UIStates.Quests:
                questFrameSelect.SetActive(S.quest.selected);

                nothingText.SetActive(!S.quest.selected);

                spellFrameSelect.SetActive(false);
                itemFrameSelect.SetActive(false);
                break;
            case UIStates.Inventory:
            case UIStates.Shop:
                itemFrameSelect.SetActive(S.item.selected);

                nothingText.SetActive(!S.item.selected);

                questFrameSelect.SetActive(false);
                spellFrameSelect.SetActive(false);
                break;
        }
    }
}
