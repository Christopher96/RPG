﻿using UnityEngine;
using System.Collections.Generic;

public class ActionbarManager : MonoBehaviour {

    public ActionbarUI UI;
	public bool isFull = false;

    public List<PlayerSpell> spells;

    void Awake() {
        emptyActionbar();
        UI.updateActionBar();
    }

    public void emptyActionbar() {
		spells = new List<PlayerSpell>(5);
    }

    public bool useSpell( PlayerSpell spell ) {
        if( !spell.isAction && !isFull ) {
            spells.Add( spell );
            UI.updateActionBar(true);
            return true;
        }

        return false;
    }

    public bool removeSpell( PlayerSpell spell ) {
        if( spell.isAction ) {
            spells.Remove( spell );
            UI.updateActionBar();
            return true;
        }

        return false;
    }
}

