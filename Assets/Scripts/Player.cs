﻿using UnityEngine;

public class Player : MonoBehaviour {

    public PlayerState state;
	public Animator anim;
    public SpriteRenderer rend;

    public float yGround;

    public Collider2D groundCol;
	public Collider2D pickupCol;
	public Collider2D contactCol;

    void Start(){
		groundCol = GetComponent <BoxCollider2D> ();
        anim = GetComponent<Animator>();
	}
		

    void Update() {
		yGround = groundCol.bounds.center.y;
		anim.SetInteger( "State", (int)state );
    }

    void changeSpritesMaterials(Material material) {
        foreach( Transform child in this.transform) {
            SpriteRenderer childRend = child.GetComponent<SpriteRenderer>();
            if( childRend != null) {
                childRend.material = material;
            }
        }
    }

    public void normal() {
		groundCol.isTrigger = false;
	}

    public bool isDead {
        get {
            return state == PlayerState.Dead;
        }
    }

    public void idle() {
        state = PlayerState.Idle;
    }

	public void attack(){
		state = PlayerState.Attacking;
	}

	public void die(){
		state = PlayerState.Dead;
	}

    public void retreat() {
        state = PlayerState.Retreating;
    }

    public void castSpell() {
        M.combat.castSpell();
    }

    public void switchTurn() {
        state = PlayerState.Idle;
        M.combat.switchTurn();
    }
}

public enum PlayerState {
    Idle, Running, Charging, Retreating, Escaping, Entering, Casting, Ranging, Attacking, Dead
}
