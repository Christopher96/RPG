﻿using UnityEngine;
using System.Collections.Generic;

public class QuestManager : MonoBehaviour {
    public Alert alert;
	public QuestUI questUI;
	
    public string getRequirementsText( Quest quest ) {

        string text = "No requirements";

        bool itemReq = false;

		if( quest.itemRequirements.Count > 0 ) {
			foreach( QuestItemRequirement req in quest.itemRequirements){
				float current = M.inventory.getAmountByItem( req.item );

				text = current.ToString() + "/" + req.amount.ToString() + " " + req.item.name;
				if( req.amount > 1 ) text += "s";
				itemReq = true;
			}
        }

		if( quest.enemyRequirements.Count > 0 ) {
            if( itemReq ) {
                text += "\t";
            } else {
                text = "";
            }

			foreach(  QuestEnemyRequirement req in quest.enemyRequirements){
				text += req.current.ToString() + "/" + req.amount.ToString() + " " + req.enemy.name;
				if( req.amount > 1 ) text += "s";
			}
        }

        return text;
    }

	public string getCompletedQuestsText(){
		int acceptedQuests = D.quest.getAcceptedQuests().Count;
		int completedQuests = D.quest.getCompletedQuests().Count;
		int totalQuests = (acceptedQuests+completedQuests);

		string text = completedQuests + " / " + totalQuests + " quest";
		if( totalQuests > 1 ) text += "s";
		text += " completed";

		return text;
	}

	public bool hasQuestRequirements( Quest quest ) {

		foreach(QuestItemRequirement req in quest.itemRequirements){
			float current = M.inventory.getAmountByItem( req.item );
			if (current < req.amount)
				return false;
		}

		foreach(QuestEnemyRequirement req in quest.enemyRequirements){
			if (req.current < req.amount)
				return false;
		}

		return true;
    }

	public void acceptQuest( Quest quest ) {
		quest.isAccepted = true;
		questUI.questList.getQuestList();
        M.UI.state = UIStates.Quests;
    }

	public void completeQuest( Quest quest ) {

		int reqSlotCount = 0;

		foreach(QuestItemRequirement req in quest.itemRequirements){
			if(req.item.has<Stackable>()) {
				Stackable stackable = req.item.get<Stackable>();
				reqSlotCount += Mathf.FloorToInt( req.amount / stackable.maxStack );
			} else {
				reqSlotCount += 1;
			}

		}

		int freeSlots = M.inventory.freeSlots + reqSlotCount;

		if ( freeSlots >= quest.itemRewards.Count ) {
			quest.isCompleted = true;


			M.inventory.totalGold += quest.goldReward;

			foreach(QuestItemRequirement req in quest.itemRequirements){
				M.inventory.removeItem (req.item, req.amount);
			}

			foreach( QuestItemReward reward in quest.itemRewards ){
				M.inventory.addItem ( reward.item, reward.amount );
			}

            M.experience.addXP(quest.xpReward);
			questUI.questList.getQuestList();
			
		} else {
			alert.prompt(
				delegate {
					M.UI.state = UIStates.Inventory;
				},
				delegate {
					M.UI.state = UIStates.Default;
				}, "Not enough space in inventory for item reward",
				"Open", "Cancel"
			);
		}
    }

    public void enemyRequirement(Enemy enemy) {
        List<Quest> quests = D.quest.getAcceptedQuests();
        string reqTxt = "";
        quests.ForEach(quest => {
            quest.enemyRequirements.ForEach(requirement => {
                if (requirement.enemy.ID == enemy.ID) {
                    if (requirement.current < requirement.amount) {
                        requirement.current += 1;
                        reqTxt = requirement.current.ToString() + "/" + requirement.amount.ToString() + " " + enemy.name;
                    }
                }
            });
        });

        M.experience.UI.reqTxt.text = reqTxt;
    }
}
