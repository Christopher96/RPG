﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armour : EquipmentItem {
    public Color color;
	public ArmourType armourType;
}

public enum ArmourType {
	Cloth, Leather, Mail, Plate
}