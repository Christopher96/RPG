﻿using UnityEngine;
using System.Collections;

public class D : MonoBehaviour {

	public static ItemDatabase item;
	public static SpellDatabase spell;
	public static QuestDatabase quest;
    public static EnemyDatabase enemy;

	public ItemDatabase _item;
	public SpellDatabase _spell;
	public QuestDatabase _quest;
    public EnemyDatabase _enemy;


    void Awake() {
		item = _item;
		spell = _spell;
		quest = _quest;
        enemy = _enemy;
	}
}
