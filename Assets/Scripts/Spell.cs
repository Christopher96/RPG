﻿using UnityEngine;
using System.Collections.Generic;

public class Spell : MonoBehaviour {
	[System.NonSerialized]
	public int ID = 0;
	public Sprite sprite {
		get {
			return GetComponent<SpriteRenderer>().sprite;
		}
	}

    public List<SpellStat> stats;
}

[System.Serializable]
public class SpellStat {
    public StatEffect effect;
	public int value;
}

public enum StatEffect {
    damage, healing
}


