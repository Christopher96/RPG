﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class ExperienceManager : MonoBehaviour {
    public ExperienceUI UI;
    float showXP;
	int showLevel;

    public int nextShowLvlXP {
        get {
            return getLvlXP( showLevel + 1 );
        }
    }

    public int totalXP = 300;
    public int level = 3;

    public int nextLvlXP {
        get {
            return getLvlXP( level + 1 );
        }
    }

    public int prevLvlXP {
        get {
            return getLvlXP( level );
        }
    }

    public int getLvlXP( int level ) {
        int nextXP = 100;

        for( int i = 0; i <= level; i++ ) {
            nextXP += 100 * ( i-1 );
        }

        return nextXP;
    }

    public Text levelText;
    public Text xpNextText;
	public Text xpText;

	public Transform xpBars;
	public Transform barPrefab;
	int maxBars;

    float startXP;
    float targetXP;
    float startTime;

    float xpTicktime = 2f;

	public GameObject xpNext;

	// Use this for initialization
	void Awake () {
        maxBars = xpBars.childCount;
		setDefaultXP();
       
    }

    bool set = false;

    void Update() {

		/*if (Time.time > 1f && !set) {
			addXP(300);
			set = true;
		}*/

		levelText.text = showLevel.ToString();

        if( showXP >= nextShowLvlXP ) {
            startXP = 0;
            targetXP -= nextShowLvlXP;
            showLevel++;
        }

        float lerp = ( Time.time - startTime ) / xpTicktime;
        showXP = Mathf.Lerp( startXP, targetXP, lerp );
        
		xpText.text = ((int)showXP).ToString() + " / " + ((int) nextShowLvlXP).ToString();
		xpNextText.text = ((int)(nextShowLvlXP - showXP)).ToString();

		float percentage = showXP / nextShowLvlXP;
		//print(percentage);

        float targetNumBars = Mathf.Ceil( percentage * maxBars );
        float numBars = xpBars.childCount;

        if( numBars > targetNumBars ) {
          
            foreach( Transform child in xpBars ) {
                Destroy( child.gameObject );
            }
        }

        if( numBars < targetNumBars ) {
            addXPBar();
        }
    }

    void addXPBar() {
        Transform bar = Instantiate( barPrefab ) as Transform;
        bar.SetParent( xpBars );
        bar.localScale = barPrefab.localScale;
        bar.localPosition = barPrefab.localPosition;
    }

	void setDefaultXP(){
        startTime = Time.time;
		showXP = startXP = targetXP = totalXP;
		showLevel = level;
	}

    public void addXP( int amount, bool showReq = false) {
        UI.reqTxt.enabled = showReq;
        UI.slideIn();

        setDefaultXP();

        totalXP += amount;

        float startDelay = 1f;

        // START TICKING
        StartCoroutine(
            Functions.wait( startDelay, () => {
                targetXP = totalXP;
            } )
        );

        float stopDelay = 1f;

        // STOP TICKING
        StartCoroutine(
            Functions.wait( xpTicktime + startDelay + stopDelay, () => {
                setDefaultXP();
                UI.slideOut();
            })
        );

        // FINAL XP
        while( totalXP >= nextLvlXP ) {
            totalXP -= nextLvlXP;
            level++;
        }
    }
}
