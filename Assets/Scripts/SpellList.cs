﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellList : MonoBehaviour {

	public Transform spellList, spellPrefab;

	void Start(){
		getSpellBook();
	}

	public void getSpellBook(){
		List<PlayerSpell> spells = D.spell.getSpells();

        foreach( Transform child in spellList ){
			Destroy( child.gameObject );
		}

		foreach( PlayerSpell spell in spells ){
            addSpellToList( spell );
		}
	}

	public void addSpellToList( PlayerSpell spell ){
		Transform spellContainer = Instantiate( spellPrefab ) as Transform;

		SpellSlot spellSlot = spellContainer.GetComponent<SpellSlot>();
		spellSlot.spell = spell;
        spellSlot.updateSlot(); 

		spellContainer.SetParent( spellList );
		spellContainer.localScale = Vector3.one;

		Vector3 pos = spellContainer.localPosition;
		pos.z = 1;
		spellContainer.localPosition = pos;
	}

	public void findAndSelectSpell( PlayerSpell spell ){

		foreach( Transform child in spellList ){
			SpellSlot spellSlot = child.GetComponent<SpellSlot>();
            Toggle toggle = child.GetComponent<Toggle>();

            toggle.isOn = (spell == spellSlot.spell);
        }
	}
}
