﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExperienceUI : MonoBehaviour {
    public bool show;
    public Text reqTxt;
    public Transform xpBar;
    Animator anim;

    Vector3 xpBarStartPos;

    void Awake() {
        xpBarStartPos = xpBar.position;
    }

    void Start() {
        anim = GetComponent<Animator>();
    }

    public void slideIn() {
        anim.enabled = true;
        M.UI.state = UIStates.Experience;
        anim.Play("slidein");
    }

    public void slideOut() {
        anim.Play("slideout");
    }

    public void disableAnim() {
        anim.enabled = false;
        xpBar.position = xpBarStartPos;
        if (M.UI.state == UIStates.Experience)
            M.UI.state = UIStates.Default;
    }
}
