﻿using UnityEngine;

public class SpellStatList : MonoBehaviour {

	public Transform spellStatPrefab, spellStatContainer;
	public GameObject nothingSelected;

	void Update(){
		nothingSelected.SetActive( !S.spell.selected );
		spellStatContainer.gameObject.SetActive( S.spell.selected );

        if( S.spell.selected )
            updatespellStats();
	}

	public void updatespellStats (){
		PlayerSpell spell = S.spell.selectedspell;

        if( spell.ID != 0 ) {
            foreach( Transform child in spellStatContainer ) {
                Destroy( child.gameObject );
            }

            addStat( "level", spell.level );

            foreach( SpellStat spellStat in spell.stats ) {
                string title = spellStat.effect.ToString().Replace( "_", " " );

                bool last = (spellStat == spell.stats[spell.stats.Count - 1]);
                addStat( title, spellStat.value, last );
            }
        }
	}

	void addStat( string title, int value, bool last = false ){

		Transform statTransform = Instantiate( spellStatPrefab ) as Transform;
		SpellStatSlot spellStatSlot = statTransform.GetComponent<SpellStatSlot>();

		spellStatSlot.title.text = title;
		spellStatSlot.value.text = value.ToString();
		if( last ) spellStatSlot.border.SetActive( false );

		statTransform.SetParent( spellStatContainer );
		statTransform.localScale = Vector3.one;
	}
}
