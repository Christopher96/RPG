﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionbarUI : MonoBehaviour {
	public Animator anim;
	public Transform actionSlots;

	Vector3 startPos;

	void Awake() {
		startPos = transform.localPosition;
		anim = GetComponent<Animator>();
	}

	void Update() {
		if(anim.enabled) {
			anim.SetBool( "showUI", M.combat.showUI );
			anim.SetBool( "playerTurn", M.combat.playerTurn );
		} else {
			transform.localPosition = startPos;
		}
	}

	public void selectLastspell() {
        for( int i = 0; i < actionSlots.childCount; i++) {
            actionSlots.GetChild(i).GetComponent<Toggle>().isOn = ( i == M.actionbar.spells.Count - 1);
        }
    }

    public void deselectAll() {
        for (int i = 0; i < actionSlots.childCount; i++) {
            actionSlots.GetChild(i).GetComponent<Toggle>().isOn = false;
        }
    }

	public void updateActionBar(bool select = false){

		List<PlayerSpell> spells = M.actionbar.spells;

		for( int i=0; i < actionSlots.childCount; i++ ){
			SpellActionSlot actionSlot = actionSlots.GetChild(i).GetComponent<SpellActionSlot> ();
			
			if( actionSlot != null ){
				if (i < spells.Count) {
					actionSlot.spell = spells [i];
				} else {
					actionSlot.spell = null;
				}
			}

            actionSlot.updateSlot();
		}

		M.actionbar.isFull = (spells.Count == actionSlots.childCount);

		if(select) selectLastspell();
		else deselectAll();
	}
}
