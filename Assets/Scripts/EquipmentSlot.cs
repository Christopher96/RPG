﻿using UnityEngine.UI;

public class EquipmentSlot : ItemSlot {

	public EquipmentType type;
	public Image slotIcon;

	new void Start() {
		base.Start();
		toggle.group = S.item.toggleGroup;
        toggle.onValueChanged.AddListener(delegate {
            S.item.selectItem(item);
        });
		slotIcon = transform.GetChild(2).GetComponent<Image>();
	}

	void Update(){
		if(item != null){
			slotIcon.enabled = false;
			itemIcon.enabled = true;
			itemIcon.sprite = item.sprite;
		} else {
			slotIcon.enabled = true;
			itemIcon.enabled = false;
		}
	}
}
