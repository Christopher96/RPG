﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class EquipmentSpriteManager : MonoBehaviour {

	public Image playerAvatarHead;
    public PlayerColorSwapper playerColorSwapper;
	public List<EquipmentRend> equipmentRends;
	public List<WeaponRend> weaponRends;

    void Update() {

		foreach(EquipmentSlot slot in M.equipment.slots){
			Item item = slot.item;

			if( item != null && item is EquipmentItem ) {

				EquipmentItem equipmentItem = item as EquipmentItem;

				if( equipmentItem != null ) {

                    if (equipmentItem is Armour) {
                        Armour armour = item as Armour;

                        switch (equipmentItem.equipmentType) {

                            case EquipmentType.Chest:
                                playerColorSwapper.updateColorSwap(ColorSwapType.Chest, armour.color);
                             break;
                            case EquipmentType.Legs:
                                playerColorSwapper.updateColorSwap(ColorSwapType.Legs, armour.color);
                            break;
                            default:
                                enableEquipmentRend(armour);
                            break;
                        }
                    } else if (equipmentItem is Weapon) {
						Weapon weapon = equipmentItem as Weapon;

						WeaponRend weaponRend = getWeaponRend(weapon.rendType);

						if(weaponRend != null) {
							disableWeaponRends();
							weaponRend.rend.enabled = true;
							weaponRend.rend.sprite = weapon.sprite;
						}						
						
					} else {
						enableEquipmentRend(equipmentItem);
					}

				} 
			} else {
				EquipmentType type = slot.type;

                switch(type) {
                    case EquipmentType.Chest:

                        break;
                    case EquipmentType.Legs:

                        break;
                    case EquipmentType.Mainhand:
                        disableWeaponRends();
                        break;
                    default:
                        disableEquipmentRend(type);
                        break;
                }
			}

			
        }
    }

	public EquipmentRend getEquipmentRend(EquipmentType type) {
        switch(type) {
            case EquipmentType.Head: return findEquipmentRend(EquipmentRendType.Head);
            case EquipmentType.Offhand: return findEquipmentRend(EquipmentRendType.Offhand);
        }

        return null;
	}

    EquipmentRend findEquipmentRend(EquipmentRendType type) {
        return equipmentRends.Find(rend => rend.type.ToString().Equals(type.ToString()));
    }

	public WeaponRend getWeaponRend(WeaponRendType type) {
		foreach(WeaponRend rend in weaponRends){
			if(rend.type == type) return rend;
		}
		return null;
	}

	void disableWeaponRends() {
		foreach(WeaponRend weaponRend in weaponRends){
			weaponRend.rend.enabled = false;
		}
	}

	void disableEquipmentRend(EquipmentType type){
		EquipmentRend equipmentRend = getEquipmentRend(type);

		if(equipmentRend != null) {
			equipmentRend.rend.enabled = false;
		}
	}

	void enableEquipmentRend(EquipmentItem item){
		EquipmentRend equipmentRend = getEquipmentRend(item.equipmentType);
		
		if(equipmentRend != null){
			equipmentRend.rend.enabled = true;
            equipmentRend.rend.sprite = item.sprite;
		}
	}
}

[System.Serializable]
public class EquipmentRend {
	public EquipmentRendType type;
	public SpriteRenderer rend;
}

public enum EquipmentRendType {
	Head, Offhand
}

[System.Serializable]
public class WeaponRend {
	public WeaponRendType type;
	public SpriteRenderer rend;
}

public enum WeaponRendType {
	Sword, Axe, Staff, Twohand, Bow
}