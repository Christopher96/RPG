﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class DialogManager : MonoBehaviour {

	public bool inDialog = false;

	public Button positiveButton;
	public Button negativeButton;

    Text positiveText;
    Text negativeText;

	public GameObject npcDialog;
	public GameObject playerDialog;

	public Text npcText;
	public Text playerText;

    DialogCollection collection;
    DialogGroup currentDialogGroup;

	public Image npcAvatar;

    void Awake() {
        positiveText = positiveButton.GetComponentInChildren<Text>();
        negativeText = negativeButton.GetComponentInChildren<Text>();
    }

	public void playDialogs( Sprite avatar, DialogCollection collection ){
        npcAvatar.sprite = avatar;
        this.collection = collection;

        if(collection.quest != null) {
            if(collection.quest.isAccepted) {
                if(playDialogGroup(DialogGroupType.QuestAccepted))
                    return;
            }
        }

        playDialogGroup(DialogGroupType.Standard);
    }

	bool playDialogGroup( DialogGroupType type ){

        DialogGroup dialogGroup = collection.dialogGroups.Find( group => group.type == type );

        if( dialogGroup != null ) {
            if( dialogGroup.dialogs.Count > 0 ) {

                dialogGroup.index = 0;
                dialogGroup.highestIndex = -1;

                currentDialogGroup = dialogGroup;
                currentDialog();
                return true;
            } else {
                Debug.LogWarning( "No dialogs set for collection" );
            }
        } else {
            Debug.LogWarning( "DialogGroup of type: " + type.ToString() + " was not found" );
        }

        return false;
	}

    IEnumerator printCoroutine;

    void currentDialog() {
        playDialog( currentDialogGroup.dialogs[currentDialogGroup.index] );
    }

    void playDialog( Dialog dialog ) {

        M.UI.state = UIStates.Dialog;

        npcDialog.SetActive( !dialog.isPlayerDialog );
        playerDialog.SetActive( dialog.isPlayerDialog );

        if(printCoroutine != null )
            StopCoroutine(printCoroutine);

        // SHOW OPTIONS BEFORE PRINT
        dialogOptions( dialog );

        // DISABLE BUTTONS BEFORE PRINT
        positiveButton.interactable = false;
        negativeButton.interactable = false;

        // ENABLE BUTTONS ON CALLBACK
        Action enableBtns = () => {
            positiveButton.interactable = true;
            negativeButton.interactable = true;

            if( currentDialogGroup.index >= currentDialogGroup.highestIndex )
                currentDialogGroup.highestIndex = currentDialogGroup.index;
        };

        if( dialog.isPlayerDialog )
            printCoroutine = printDialog( playerText, dialog, enableBtns );
        else
            printCoroutine = printDialog( npcText, dialog, enableBtns );

        StartCoroutine(printCoroutine);
    }

    void nextDialog() {

        if ( currentDialogGroup.index < currentDialogGroup.dialogs.Count -1 ) {
            currentDialogGroup.index++;
            currentDialog();
        } else {
            M.UI.state = UIStates.Default;
        }
    }

    void prevDialog() {
        if( currentDialogGroup.index > 0 ) {
            currentDialogGroup.index--;
            currentDialog();
        } else {
            M.UI.state = UIStates.Default;
        }
    }

	IEnumerator printDialog( Text textField, Dialog dialog, Action callback ){
		textField.text = "";

        if ( dialog.text != null ){
            if( currentDialogGroup.index <= currentDialogGroup.highestIndex || dialog.isInstant ) {
                // INSTANT PRINT
                textField.text = dialog.text;

            } else  {
				// SUCCESSIVE PRINT
				for( int letterIndex = 0; letterIndex < dialog.text.Length; letterIndex++ ){

					float waitTime = 0.02f;

					string nextChar = dialog.text.Substring( letterIndex, 1 );
					textField.text = textField.text + nextChar;

					switch( nextChar ) {
					case ",": waitTime = 0.2f; break;
					case ".":
					case "?":
					case "!":
						waitTime = 0.5f; 
						break;
					}


					if( letterIndex != dialog.text.Length-1 ){
						yield return new WaitForSeconds( waitTime );
					}

				}
			}
		}

        callback();
    }

    void questCheck() {
        if (collection.quest.isAccepted) {
            if (collection.quest.isCompleted) {
                playDialogGroup(DialogGroupType.QuestCompleted);                        
            } else {
                if (M.quest.hasQuestRequirements(collection.quest)) {
                    playDialogGroup(DialogGroupType.QuestReward);
                } else {
                    if(playDialogGroup(DialogGroupType.QuestNoreward) == false) {
                        nextDialog();
                    }
                }
            }
        } else {
            nextDialog();
        }
    }

    void dialogOptions( Dialog dialog ) {

        positiveButton.onClick.RemoveAllListeners();
        negativeButton.onClick.RemoveAllListeners();

        if(dialog.positiveText.Equals("")){
            switch (dialog.positiveAction) {
                case PositiveDialogAction.QuestAccept: positiveText.text = "Accept"; break;
                case PositiveDialogAction.QuestComplete: positiveText.text = "Complete"; break;
                case PositiveDialogAction.QuestDialog: 
                case PositiveDialogAction.QuestCheck: 
                    positiveText.text = "Quest"; break;
                case PositiveDialogAction.Shop: positiveText.text = "Shop"; break;
                case PositiveDialogAction.Ok: positiveText.text = "Ok"; break;
                default: positiveText.text = "Next"; break;
            }
        } else {
            positiveText.text = dialog.positiveText;
        }

        positiveButton.onClick.AddListener(delegate {
            switch ( dialog.positiveAction ) {

                case PositiveDialogAction.NextDialog:
                    nextDialog();
                break;

                case PositiveDialogAction.QuestCheck:
                    questCheck();
                break;

                case PositiveDialogAction.QuestAccept:
                    M.quest.acceptQuest(collection.quest);
                break;

                case PositiveDialogAction.QuestComplete:
                     M.quest.completeQuest(collection.quest);
                break;

                case PositiveDialogAction.QuestDialog:
                    playDialogGroup(DialogGroupType.QuestDialog);
                break;

                case PositiveDialogAction.Shop:
                    M.shop.setShop(collection.shop);
                    M.UI.state = UIStates.Shop;
                break;

                default:
                    M.UI.state = UIStates.Default;
                break;
            }
        });

        if(dialog.negativeText.Equals("")){
            switch (dialog.negativeAction) {
                case NegativeDialogAction.Shop: negativeText.text = "Shop"; break;
                case NegativeDialogAction.Decline: negativeText.text = "Decline"; break;
                case NegativeDialogAction.Cancel: negativeText.text = "Cancel"; break;
                case NegativeDialogAction.No: negativeText.text = "No"; break;
                case NegativeDialogAction.Leave: negativeText.text = "Leave"; break;
                default: negativeText.text = "Previous"; break;
            }
        } else {
            negativeText.text = dialog.negativeText;
        }

        negativeButton.onClick.AddListener(delegate {
            switch (dialog.negativeAction) {

                case NegativeDialogAction.PrevDialog:
                    prevDialog();
                break;

                case NegativeDialogAction.Shop:
                    M.shop.setShop(collection.shop);
                    M.UI.state = UIStates.Shop;
                break;

                default:
                    M.UI.state = UIStates.Default;
                break;
            }
        });

    }

}
