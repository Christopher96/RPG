﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDatabase : MonoBehaviour {
    public int globalID = 0;

    void Awake() {
        Resources.LoadAll("Enemy");
        foreach (Enemy enemy in Resources.FindObjectsOfTypeAll<Enemy>()) {
            enemy.ID = globalID++;
        }
    }
}
