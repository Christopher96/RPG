using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class ShopManager : MonoBehaviour {

	public ShopTab tab;
	public Shop shop;

	public Transform shopContainer, listItemPrefab;
    public Toggle equipmentTab, consumableTab;
	public GameObject generalTab;

	void tabCheck() {
		bool hasConsumables = false;
		bool hasEquipment = false;
		foreach(Tradable tradable in shop.table) {
			Item item = tradable.GetComponent<Item>();
			if(item is EquipmentItem) hasEquipment = true;
			else if(item is ConsumableItem) hasConsumables = true;
		}

		bool notGeneralStore = hasConsumables && hasEquipment;
		if(notGeneralStore) {
			equipmentTab.onValueChanged.AddListener(delegate {
				if(equipmentTab.isOn){
					tab = ShopTab.Equipment;
				} else {
					tab = ShopTab.Consumables;
				}

				tabSort();
			});

			tabSort();
		}

		equipmentTab.gameObject.SetActive(notGeneralStore);
		consumableTab.gameObject.SetActive(notGeneralStore);
		generalTab.SetActive(!notGeneralStore);
	}

	void tabSort() {
		foreach( Transform slotPrefab in shopContainer ){
			ItemSlot slot = slotPrefab.GetComponent<ItemSlot>();
			if(S.item.selectedSlot != slot && slot.toggle.isOn) slot.toggle.isOn = false;
			slotPrefab.gameObject.SetActive(
				(slot.item is EquipmentItem && tab == ShopTab.Equipment) ||
				(slot.item is ConsumableItem && tab == ShopTab.Consumables)
			);
		}
	}

	public void setShop(Shop shop) {
		this.shop = shop;

		if( shop != null && shop.table.Count > 0 ){

			foreach( Transform child in shopContainer ) {
				Destroy( child.gameObject );
			}

			foreach( Tradable tradable in shop.table ){
				Item item = tradable.GetComponent<Item>();
				if( item != null ){
					Transform shopSlotContainer = Instantiate( listItemPrefab ) as Transform;
					ListItemSlot shopSlot = shopSlotContainer.GetComponent<ListItemSlot>();

					shopSlot.toggle.group = S.item.toggleGroup;
					shopSlot.toggle.onValueChanged.AddListener(delegate {
						S.item.selectItem(item);
					});

					shopSlot.item = item;
					shopSlotContainer.SetParent( shopContainer );
					shopSlotContainer.transform.localScale = Vector3.one;
				}
			}
		}

		tabCheck();
	}

	public void findAndSelectSlot( Item item ){
		foreach( Transform slotPrefab in shopContainer ){
			ItemSlot slot = slotPrefab.GetComponent<ItemSlot>();
			if( slot.item.ID == item.ID ){
                Functions.click( slotPrefab );
			}
		}
	}

	public bool buyItem(Item item) {
		if(item.GetComponent<Tradable>() != null) {
			if (M.inventory.addItem(item)) {
				M.inventory.totalGold -= item.GetComponent<Tradable>().buyPrice;
				return true;
			}
		}


		return false;
	}

	public void sellItem(Tradable tradable) {
		M.inventory.totalGold += tradable.sellPrice;
	}
		
}


public enum ShopTab {
	Equipment, Consumables
}







