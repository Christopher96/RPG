﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatBonusText : MonoBehaviour {

	public CombatStatType type;
	public Text bonusText;

	void Awake () {
		bonusText = GetComponent<Text>();
	}
	
	void Update () {
		bonusText.text = getVisuaBonusText();
	}

	public string getVisuaBonusText(){
		int bonus = M.stat.getStat(type).visualBonus;
		
        if(bonus > 0){
            return "+"+bonus.ToString();
        } else if(bonus == 0){
            return "";
        } else {
            return bonus.ToString();
        }
    }
}
