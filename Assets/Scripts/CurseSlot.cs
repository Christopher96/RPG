﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurseSlot : MonoBehaviour {
    public Curse curse;
    public Text duration;
    public Image icon;

	void Update () {
        updateSlot();
	}

    public void updateSlot() {
        duration.text = curse.duration.ToString();
        icon.sprite = curse.sprite;
    }
}
