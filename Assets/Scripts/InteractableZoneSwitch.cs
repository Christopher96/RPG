﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class InteractableZoneSwitch : InteractableButton {

    public SceneField zone;
    public ZoneSpawnType spawn;

    public override void interact() {
        M.zone.moveToZone(zone,spawn);
    }
}
