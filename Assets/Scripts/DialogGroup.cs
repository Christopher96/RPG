﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogGroup {
    public DialogGroupType type;
    public List<Dialog> dialogs;

    [System.NonSerialized]
    public int index = 0;
    [System.NonSerialized]
    public int highestIndex = -1;
}

public enum DialogGroupType {
    Standard, QuestDialog, QuestReward, QuestNoreward, QuestAccepted, QuestCompleted
}