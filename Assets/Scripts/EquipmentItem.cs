﻿[System.Serializable]
public class EquipmentItem : StatItem {
	public EquipmentType equipmentType;
}

public enum EquipmentType {
	Head, Chest, Legs, Boots, Offhand, Neck, Ring, Mainhand
}

