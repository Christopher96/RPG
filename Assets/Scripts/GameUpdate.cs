﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameUpdate : MonoBehaviour {

	EventSystem eventSystem;
	public List<GameObject> dontDestroyOnLoad;

	void Awake() {
		eventSystem = GetComponentInChildren<EventSystem>();
		eventSystem.pixelDragThreshold = 10;

		foreach (GameObject gObject in dontDestroyOnLoad) {
			DontDestroyOnLoad(gObject);
			gObject.SetActive(true);
		}
	}

	void Update() {
		if (Input.GetKeyDown("n")) {
            print("new game");
			SaveManager.NewGame();
		}

		if (Input.GetKeyDown("s")) {
            print("save");
			SaveManager.SaveGame();
		}


		if (Input.GetKeyDown("l")) {
            print("load");
			SaveManager.LoadList();
			SaveManager.LoadGame(SaveManager.GameList[0]);
		}

		if (Input.GetKeyDown("d")) {
            print("delete");
			SaveManager.DeleteList();
		}
	}
}
