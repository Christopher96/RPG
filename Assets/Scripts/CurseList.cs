﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CurseList : MonoBehaviour {
    public bool isPlayer;
    public Transform curseSlotPrefab;

    void Start() {
        emptyCurseList();
    }

    public void updateList() {
        if (isPlayer) {
            generateCurseList(M.stat.curses.Cast<Curse>().ToList());
        } else {
            generateCurseList(M.combat.enemy.curses.Cast<Curse>().ToList());
        }
    }

    public void generateCurseList(List<Curse> curses) {
        emptyCurseList();

        foreach (Curse curse in curses) {
            if (curse.duration < 1) continue;

            Transform curseSlotContainer = Instantiate(curseSlotPrefab) as Transform;
            CurseSlot curseSlot = curseSlotContainer.GetComponent<CurseSlot>();

            curseSlot.curse = curse;
            curseSlot.updateSlot();
            curseSlotContainer.SetParent(this.transform);
            Vector3 scale = Vector3.one;
            if(!isPlayer) {
                scale.x = -1;
            }
            curseSlotContainer.localScale = scale;
        }

    }

    public void emptyCurseList() {
        foreach(Transform child in this.transform) {
            Destroy(child.gameObject);
        }
    }
}
