﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ZoneSwitch : MonoBehaviour {

	public SceneField zone;
    public ZoneEdgeType edgeType;
    public ZoneSpawnType dstSpawn;

    void OnTriggerExit2D( Collider2D col ){
        if ( col.tag.Equals("Player") && M.camera.onEdge ) {
            M.zone.moveToZone( zone, dstSpawn );
        }
	}
}
