﻿
[System.Serializable]
public class PlayerStat {

    public PlayerStat( PlayerStatType _stat, int _value ) {
        this.type = _stat;
        this.value = _value;
    }

	public PlayerStatType type;
	public int value;

}
	
public enum PlayerStatType {
	Defence, Strength, Range, Magic
}