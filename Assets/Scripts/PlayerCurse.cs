﻿public class PlayerCurse : Curse {
	public PlayerCurseType type;
}

public enum PlayerCurseType {
	Hitpoints, Attack, Defence, Stun
}