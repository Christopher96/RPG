﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuUI : MonoBehaviour {

	public Button saveBtn;
	public Text saveBtnTxt;

	public void openMenu() {
		M.UI.state = UIStates.Menu;
		saveBtn.interactable = true;
		saveBtnTxt.text = "Save";
	}

	public void saveGame() {
		SaveManager.SaveGame();
		saveBtn.interactable = false;
		saveBtnTxt.text = "Saved!";
	}
}
