﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogCollection {

	public List<DialogGroup> dialogGroups;

	public Shop shop;
	public Quest quest;
}
