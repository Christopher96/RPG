﻿using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour {
	public Transform _slots;
	public List<EquipmentSlot> slots;

	public EquipmentManager() {
		slots = new List<EquipmentSlot>();
	}

	void Awake() {
		foreach (Transform child in _slots) {
			slots.Add(child.GetComponent<EquipmentSlot>());
		}
	}

	public void deselectAll() {
		slots.ForEach(slot => slot.toggle.isOn = false);
	}

	public void findAndSelectItem(EquipmentItem item) {
		slots.Find(slot => slot.item == item).toggle.isOn = true;
	}

	public EquipmentSlot getEquipmentSlot(EquipmentType type) {
		return slots.Find(slot => slot.type == type);
	}

	public EquipmentItem getEquipmentSlotItem(EquipmentType type) {
		return getEquipmentSlot(type).item as EquipmentItem;
	}

	public bool isEquiped(EquipmentType type) {
		return getEquipmentSlot(type).item != null;
	}

	public bool isEquiped(EquipmentItem item) {
		return getEquipmentSlot(item.equipmentType).item == item;
	}

    public void emptyEquipment() {
        foreach(EquipmentSlot slot in slots) {
			if(slot.item != null) {
            	slot.item.Delete();
				slot.item = null;
			}
        }
    }

	public bool canEquip(EquipmentItem item) {
		if (item is Weapon) {
			Weapon weapon = item as Weapon;

			if (weapon.isTwoHand) {
				if (isEquiped(EquipmentType.Offhand)) {
					if (M.inventory.freeSlots < 1) {
						return false;
					}
				}
			}
		} else if (item.equipmentType == EquipmentType.Offhand) {
			if (twoHandIsEquiped()) {
				if (M.inventory.freeSlots < 1) {
					return false;
				}
			}
		}

		return true;
	}

	public bool twoHandIsEquiped() {
		if (isEquiped(EquipmentType.Mainhand)) {
			EquipmentItem mainHand = getEquipmentSlotItem(EquipmentType.Mainhand);

			if (mainHand is Weapon) {
				if ((mainHand as Weapon).isTwoHand) {
					return true;
				}
			}
		}

		return false;
	}

	public bool canUnEquip(EquipmentItem item) {
		if (!M.inventory.isFull && isEquiped(item)) {
			return true;
		}

		return false;
	}

	public bool equipItem(EquipmentItem item, bool select = true) {
		if (!canEquip(item)) return false;

		EquipmentItem newItem = item.Clone() as EquipmentItem;
		
		if(M.inventory.hasUniqueItem(item)) {
			M.inventory.removeUniqueItem(item);
		}

		if (newItem is Weapon) {
			Weapon weapon = newItem as Weapon;

			if (weapon.isTwoHand) {
				unEquipSlot(EquipmentType.Offhand);
			}

			M.actionbar.UI.updateActionBar();
		} else if (newItem.equipmentType == EquipmentType.Offhand && twoHandIsEquiped()) {
			unEquipSlot(EquipmentType.Mainhand);
		}

		unEquipSlot(newItem.equipmentType);

		EquipmentSlot slot = getEquipmentSlot(newItem.equipmentType);
		slot.item = newItem;

		if (select)
			slot.toggle.isOn = true;

		M.stat.addStats(newItem.stats);

		return true;
	}

	public bool unEquipItem(EquipmentItem item) {
		if (canUnEquip(item)) {
			unEquipSlot(item.equipmentType);
		}

		return false;
	}

	public bool unEquipSlot(EquipmentType type) {
		if (isEquiped(type)) {
			EquipmentItem item = getEquipmentSlot(type).item as EquipmentItem;

			if (M.inventory.addItem(item, true)) {
				removeEquiped(type);
				return true;
			}
		}

		return false;
	}

	public void removeEquiped(EquipmentType type, bool drop = false) {
		EquipmentSlot slot = getEquipmentSlot(type);
		M.stat.removeStats((slot.item as EquipmentItem).stats);
		if (drop) slot.item.Drop();
		else slot.item.Remove();
	}
}
