﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : MonoBehaviour {

	void Awake() {
		if(PlayerPrefs.HasKey("settings_joystick")) {
			int settings_joystick = PlayerPrefs.GetInt("settings_joystick");
			JoystickController.isLefty = (settings_joystick == 1) ? true : false;
		}
	}

	public void setBool(string name, bool val) {
		PlayerPrefs.SetInt("settings_joystick", val ? 1 : 0);
	}
}
