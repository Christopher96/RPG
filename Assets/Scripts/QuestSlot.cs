﻿using UnityEngine;
using UnityEngine.UI;

public class QuestSlot : MonoBehaviour {

	public Image icon;
	public Text title;
	public Quest quest;
    public Toggle toggle;

    void Start() {
        toggle.group = S.quest.toggleGroup;
    }

	void Update(){
		if(quest != null){
			title.text = quest.name;
			icon.sprite = quest.sprite;
		}
	}
}
