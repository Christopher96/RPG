﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public Font globalFont;
    public static Font _globalFont;

    int totalStates;
    public UIStates firstState;

    public List<GameObject> UIPanes;

    public UIStates _state;
    public UIStates state {
        get {
            return _state;
        }
        set {
            _state = value;
            stateUpdate();
        }
    }

    public Text stateTitle;
    public Toggle inventoryToggle;

    void Awake() {
        firstState = UIStates.Inventory;
        totalStates = Enum.GetNames(typeof(UIStates)).Length;
        _globalFont = globalFont;

        inventoryToggle.onValueChanged.AddListener(delegate {
            if (inventoryToggle.isOn) {
                state = UIStates.Inventory;
            } else {
                state = UIStates.Default;
            }
        });
    }

    public void stateUpdate() {
        stateTitle.text = state.ToString();

        List<string> EnabledUIs = new List<string>();

        switch (state) {
            case UIStates.Menu:
            case UIStates.Settings:
            case UIStates.Closed:
            case UIStates.Combat:
            case UIStates.Dialog:
            case UIStates.Experience:
                break;
            default:
                EnabledUIs.Add("ButtonsUI");
                break;
        }

        switch (state) {
            case UIStates.Closed:
                break;

            case UIStates.Menu:
                EnabledUIs.AddRange(new string[] { "MenuUI" });
                break;

            case UIStates.Settings:
                EnabledUIs.AddRange(new string[] { "DefaultUI", "SettingsUI" });
                break;
                
            case UIStates.Default:
                EnabledUIs.AddRange(new string[] { "DefaultUI" });
                break;

            case UIStates.Experience:
                EnabledUIs.AddRange(new string[] { "DefaultUI", "ExperienceUI" });
                break;

            case UIStates.Combat:
                EnabledUIs.AddRange(new string[] { "CombatUI", "ActionbarUI" });
                break;

            case UIStates.Dialog:
                EnabledUIs.AddRange(new string[] { "DialogUI" });
                break;

            case UIStates.Shop:
                EnabledUIs.AddRange(new string[] { "InventoryUI", "ShopUI", "FrameUI", "ExperienceUI" });
                break;

            case UIStates.Loot:
                EnabledUIs.AddRange(new string[] { "LootUI" });
                break;

            case UIStates.Inventory:
                EnabledUIs.AddRange(new string[] { "InventoryUI", "EquipmentUI", "FrameUI", "ExperienceUI" });
                break;

            case UIStates.Quests:
                EnabledUIs.AddRange(new string[] { "QuestUI", "FrameUI", "ExperienceUI" });
                break;

            case UIStates.Spells:
                EnabledUIs.AddRange(new string[] { "SpellUI", "ActionbarUI", "FrameUI", "ExperienceUI" });
                break;

            case UIStates.Alert:
                EnabledUIs.AddRange(new string[] { "AlertUI" });
                break;

        }

        foreach (GameObject UIPane in UIPanes) {
            UIPane.SetActive(EnabledUIs.Contains(UIPane.name));
        }
    }

    public UnityEvent lootClose;

    public void closeUI() {
        if (state == UIStates.Loot) {
            lootClose.Invoke();
        }
        state = UIStates.Default;
        inventoryToggle.isOn = false;
    }

    public void prevState() {
        int stateIndex = (int) state;

        if (stateIndex > (int) firstState) {
            stateIndex--;
            state = (UIStates) stateIndex;
        } else {
            state = (UIStates) totalStates - 1;
        }
    }

    public void nextState() {
        int stateIndex = (int) state;

        if (stateIndex < totalStates-1) {
            stateIndex++;
            state = (UIStates) stateIndex;
        } else {
            state = (UIStates) firstState;
        }
    }
}

public enum UIStates {
    Menu,
    Settings,
    Default,
    Closed,
    Experience,
    Combat,
    Dialog,
    Shop,
    Loot,
    Alert,
    Inventory,
    Spells,
    Quests
}