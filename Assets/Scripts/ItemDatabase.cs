﻿using UnityEngine;
using System.Collections.Generic;

public class ItemDatabase : MonoBehaviour {

	public Transform itemsContainer;
	public List<RarityColor> RarityColors;

	public List<Item> Items;
	public int globalID = 0;

	void Awake(){
		Resources.LoadAll("Items");
		foreach(Item item in Resources.FindObjectsOfTypeAll<Item>()){
			item.ID = ++globalID;
			Items.Add(item);
		}
	}
		
	public Color32 getRarityColor( Rarity rarity ) {
		return RarityColors.Find(rColor => rColor.rarity == rarity).color;
	}

	public Item getItemByTitle( string title ){
        title = title.ToLower();

        if( title != null && !title.Equals("") ){
            Item result = Items.Find( item => (item.name.ToLower().Equals( title )) );
            if( result != null ) {
                return result;
            } else {
                return new Item();
            }
        } else {
            return new Item();
        }
		
	}

	public Item getItemById(int id) {
		return Items.FindLast(item => item.ID == id);
	}

}

[System.Serializable]
public class RarityColor {
	public Rarity rarity;
	public Color32 color;
}






