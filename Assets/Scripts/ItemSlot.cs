﻿using UnityEngine;
using UnityEngine.UI;

public class  ItemSlot : MonoBehaviour {

	public Item item;
	public Toggle toggle;
	public Image itemIcon;

	public void Start() {
		toggle = GetComponent<Toggle>();
		if(toggle.graphic == null) toggle.graphic = transform.GetChild(0).GetComponent<Image>();
		if(itemIcon == null) itemIcon = transform.GetChild(1).GetComponent<Image>();
	}
}
