﻿using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class CustomSelectable : Selectable {

    public Action down;
    public Action up;

    public override void OnPointerUp(PointerEventData eventData) {
        up();
    }

    public override void OnPointerDown(PointerEventData eventData) {
        down();
    }
}
