﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;

public class ZoneManager : MonoBehaviour {

    public bool isLoading = false;
	public Image loading;

    public string currentZone;
    public SceneField startZone;
    public ZoneSpawnType zoneSpawn;

	public float Xoffset = 0.45f;
	public float Yoffset = -0.1f;

    void Start() {
        moveToZone( startZone, zoneSpawn );
    }

    IEnumerator loadLevel( string scene, Action callback){

        isLoading = true;

        AsyncOperation async = SceneManager.LoadSceneAsync( scene );
        async.allowSceneActivation = false;

        fadeIn(() => {
            async.allowSceneActivation = true;
        } );

        while( !async.isDone ) {
            yield return null;
        }

        callback();

        fadeOut(() => {
            isLoading = false;
        });
	}

    void fadeIn( Action callback ) {
        Color color = loading.color;
        loading.gameObject.SetActive( true );

        StartCoroutine(
           Tween( 0, 1f,
               ( newvalue ) => {
                   color.a = newvalue;
                   loading.color = color;
               }, callback
           )
       );
    }

    void fadeOut( Action callback ) {
        Color color = loading.color;

        StartCoroutine(
            Tween( 1f, 0,
                ( newvalue ) => {
                    color.a = newvalue;
                    loading.color = color;
                }, () => {
                    loading.gameObject.SetActive( false );
                    callback();
                }
            )
       );
    }

    IEnumerator Tween( float current, float target, Action<float> update, Action callback ) {

        bool increase = (current < target);
        float increment = 0.05f;

        if( increase ) {
            while( current < target ) {

                current += increment;
                update( current );

                yield return null;
            }
        } else {
            while( current > target ) {

                current -= increment;
                update( current );

                yield return null;
            }
        }
        

        callback();
    }

    public bool moveToZone( SceneField scene, ZoneSpawnType spawnType = ZoneSpawnType.Left ){
        return moveToZone(scene.SceneName, spawnType);
    }

	public bool moveToZone( string scene, ZoneSpawnType spawnType = ZoneSpawnType.Left, Action callback = null ){

        currentZone = scene;

		if ( Application.CanStreamedLevelBeLoaded( scene ) ) {
            if(M.UI.state != UIStates.Combat)
                M.UI.state = UIStates.Default;

            M.loot.wipeGroundLoot();

			StartCoroutine(
                loadLevel( scene, () => {
                    List<ZoneSpawn> spawns = new List<ZoneSpawn>(FindObjectsOfType<ZoneSpawn>());
                    ZoneSpawn spawn = spawns.Find(_spawn => _spawn.type.Equals(spawnType));
                    if(spawn != null) spawn.spawn();

                    M.camera.updateEdges();
                    M.camera.updatePosition();
                    
                    if(callback != null) {
                        callback();
                    }
                })
            );

			return true;
			
		} else {
    			Debug.LogWarning( scene + " does not exist or is not added to build settings" );
			return false;
		}
	}
}

