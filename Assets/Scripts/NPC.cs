﻿using UnityEngine;
using System.Collections.Generic;

public class NPC : InteractableButton {
	public Sprite avatar;
	public DialogCollection dialogCollection;

	void Start(){
		if( interactText.Equals("") )interactText = "tap to speak with " + name;
	}

	public override void interact (){
		M.dialog.playDialogs( avatar, dialogCollection );
	}
		
}
