﻿using UnityEngine;

public class Item : MonoBehaviour {
	public int ID = 0;

	public string description;

	public Sprite sprite {
		get {
			return GetComponent<SpriteRenderer>().sprite;
		}
	}

    public Rarity rarity;

	public Item Clone() {
		Item newItem = Instantiate(this, D.item.itemsContainer);
		newItem.name = this.name;
		return newItem;
	}

	public void Drop() {
		M.loot.spawnItem(M.player.transform.position.x, this);
		this.Remove();
	}

	public void Remove(){

		bool delete = true;
		
		if (this.has<Stackable>()) {
			Stackable stackable = this.get<Stackable>();
			if (stackable.amount > 0) {
				stackable.amount--;
			}
			
			delete = stackable.amount <= 0;
		}
		
		if(delete) {
			Delete();
		}
	}

	public void Delete() {
		Destroy(this.gameObject);
	}

	public bool has<T>() {
		if(GetComponent<T>() != null){
			return true;
		}

		return false;
	}

	public T get<T>(){
		return GetComponent<T>();
	}
}

public enum Rarity {
	Misc, Common, Rare, Epic
}









