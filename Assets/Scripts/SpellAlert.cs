﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpellAlert : MonoBehaviour {

	public Image topBanner, bottomBanner, spellIcon;
	public Color playerColor, enemyColor;
	public Text characterText, spellText;

	// Use this for initialization
	public void init ( Spell spell ) {
		Vector3 scale = Vector3.one;

        transform.localScale = scale;
        characterText.transform.localScale = scale;
        spellText.transform.localScale = scale;

		spellIcon.sprite = spell.sprite;
        spellText.text = spell.name;

        if ( spell is PlayerSpell ){

			characterText.text = "Player is casting";
			topBanner.color = playerColor;
			bottomBanner.color = playerColor;
		} else if ( spell is EnemySpell ) {
			scale.x *= -1;

			characterText.text = M.combat.enemy.name + " is casting";
			topBanner.color = enemyColor;
			bottomBanner.color = enemyColor;
		}


		GetComponent<Animator>().SetTrigger("animate");
	}
}
