﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour {

	public BoxCollider2D groundCol;
	private SpriteRenderer rend;

    private void Awake() {
		rend = GetComponent<SpriteRenderer> ();
    }

    void Update() {
		if( groundCol != null && !M.combat.inCombat ) {
			float yGround = groundCol.bounds.center.y;
			rend.sortingLayerName = (M.player.yGround > yGround) ?  "PlayerFront" : "PlayerBack";
		}
	}
}
