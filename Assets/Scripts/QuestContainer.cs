﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestContainer : MonoBehaviour {

	public Text goldXPReward;
	public Text description;

	public GameObject rewardList;
	public GameObject rewardSlot;

	public GameObject noItemRewards;

	Quest lastQuest;

	void Update(){
		Quest quest = S.quest.selectedQuest;

		if(S.quest.selected && lastQuest != quest){

			lastQuest = quest;

			string goldxpText = "";
			if( quest.xpReward > 0 ) {
				goldxpText += quest.xpReward.ToString() + " XP";

				if( quest.goldReward > 0 ) {
					goldxpText += "\t";
				}
			}

			if( quest.goldReward > 0 ) {
				goldxpText += quest.goldReward.ToString() + " GOLD";
			}

			goldXPReward.text = goldxpText;
			description.text = quest.description;

			foreach (Transform child in rewardList.transform) {
				Destroy (child.gameObject);
			}

			foreach (QuestItemReward reward in quest.itemRewards) {
				Vector3 scale = rewardSlot.transform.lossyScale;
				GameObject questReward = Instantiate (rewardSlot) as GameObject;
				questReward.transform.SetParent (rewardList.transform);
				questReward.transform.localScale = scale;
				questReward.GetComponent<QuestRewardSlot> ().instantiate (reward.item, reward.amount);
			}

			noItemRewards.SetActive( quest.itemRewards.Count < 1 );

		}

	}
}
