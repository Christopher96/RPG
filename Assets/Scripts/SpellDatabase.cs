﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpellDatabase : MonoBehaviour {

	private int globalID = 0;
	public List<Spell> spells;

	public Color32 magicSpellColor;
	public Color32 rangeSpellColor;
	public Color32 meleeSpellColor;

	private int curseID = 0;
	private int buffID = 0;

	public SpellDatabase() {
		spells = new List<Spell>();		
	}

	void Awake(){
		Resources.LoadAll("Spells");
		foreach(Spell spell in Resources.FindObjectsOfTypeAll<Spell>()){
			spell.ID = ++globalID;
			spells.Add(spell);
            copySprite(spell);
		}

		Resources.LoadAll("Curses");
		foreach(Curse curse in Resources.FindObjectsOfTypeAll<Curse>()){
			curse.ID = ++curseID;
		}

		Resources.LoadAll("Buffs");
		foreach(Buff buff in Resources.FindObjectsOfTypeAll<Buff>()){
			buff.ID = ++buffID;
		}
	}

	public List<PlayerSpell> getSpells(){
		return spellsByType<PlayerSpell>().FindAll( spell => ( spell.level <= M.experience.level && !spell.isAction ) );
	}

	public Spell getSpellById(int id) {
		return spells.FindLast(spell => spell.ID == id);
	}

	public List<T> spellsByType<T>() {
		List<T> newSpells = new List<T>();
		spells.FindAll(spell => spell is T).ForEach(spell => newSpells.Add((T)(object)spell));
		return newSpells;
	}

    public void copySprite(Spell spell) {
        if(spell is PlayerSpell) {
            PlayerSpell playerSpell = spell as PlayerSpell;

            foreach (PlayerCurse curse in playerSpell.curses) {
                curse.sprite = playerSpell.sprite;
            }
        } else if(spell is EnemySpell) {
            EnemySpell enemySpell = spell as EnemySpell;

            foreach (EnemyCurse curse in enemySpell.curses) {
                curse.sprite = enemySpell.sprite;
            }
        }
    }
}











