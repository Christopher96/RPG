﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Alert : MonoBehaviour {

    public Button positiveButton, negativeButton;
    public Text descriptionText;

    public void prompt( UnityAction positiveAction, UnityAction negativeAction, string description, string positiveText = "Yes", string negativeText = "No" ) {
        M.UI.state = UIStates.Alert;

        descriptionText.text = description;

        positiveButton.GetComponentInChildren<Text>().text = positiveText;
        negativeButton.GetComponentInChildren<Text>().text = negativeText;

        positiveButton.onClick.RemoveAllListeners();
        negativeButton.onClick.RemoveAllListeners();

        positiveButton.onClick.AddListener( positiveAction );
        negativeButton.onClick.AddListener( negativeAction );

    }
}
