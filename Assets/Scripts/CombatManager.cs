﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;

public class CombatManager : MonoBehaviour {

    public CombatUI UI;
    public bool isFinished = false;
    public bool playerTurn = false;
    public bool inCombat = false;
    public bool showUI = false;

    public Enemy enemy;

    public List<Scene> clearedZones;

    public Button exitButton;

    public Respawn respawn;
    public CombatUI combatUI;

    public HealthAlert playerHealthAlert;
    public HealthAlert enemyHealthAlert;


	void Start(){
        clearedZones = new List<Scene>();
	}

	void Update(){
        if( inCombat ){
            updateEnemyStats();
            exitButton.interactable = playerTurn;
		}
	}

    public void enemyCheck(Enemy _enemy) {
        enemy = _enemy;
        if(!enemy.isDead) {
            enterCombat();
        }
    }

	public void spellAlert( Spell spell, Action callback ){
		combatUI.spellAlert.init( spell );
        StartCoroutine(
            Functions.wait( 1.5f, () => {
                if(callback != null) callback();
            })
        );
	}

	

    public void initiateCombat(){
        if( enemy != null ) {
            // Reset variables
            showUI = true;
            inCombat = true;
            playerTurn = true;
            isFinished = false;
            M.movement.isFlipped = false;
            M.camera.passed = false;

            // Reset HP
			M.stat.resetHP();
            updateHealthBars();
            
            // Enable actionbar anim
            M.actionbar.UI.anim.enabled = true;

            // Change enemy avatar sprite
            combatUI.enemyFrame.avatar.sprite = enemy.avatar;

            // Update curse lists
            M.stat.curseUpdate();
            enemy.curseUpdate();

            M.UI.state = UIStates.Combat;
        }
    }

    public void escapeCombat() {
        showUI = false;
        playerTurn = false;
        M.player.state = PlayerState.Escaping;

        StartCoroutine(
            Functions.wait( 1.5f, () => {
                inCombat = false;
                showUI = false;
                M.camera.passed = false;
                M.player.idle();
            })
        );
    }

    public void enterCombat() {
        initiateCombat(); 

        enemy.groundCol.isTrigger = true;

        M.actionbar.UI.deselectAll();
        M.movement.startEntering();
			
        StartCoroutine(
            Functions.wait(0.8f, () => {
                readyCombat();
            })
        );
    }

    public void readyCombat() {
        M.player.idle();
        M.movement.startX = M.player.transform.position.x;

        // Move healthalerts
        Vector2 playerPos = Camera.main.WorldToScreenPoint(M.player.transform.position);
        playerHealthAlert.transform.position = playerPos;
        Vector2 enemyPos = Camera.main.WorldToScreenPoint(enemy.transform.position);
        enemyPos.y = playerPos.y;
        enemyHealthAlert.transform.position = enemyPos;
    }

    public void slowEndCombat() {
        StartCoroutine(
            Functions.wait(1f, () => {
                endCombat();
            })
        );
    }
	public void endCombat(){
        showUI = false;
        playerTurn = false;

        M.player.normal();
        enemy.normal();

        if (M.player.isDead)
            Instantiate(respawn.gameObject);
    }


	public void playTurn( PlayerSpell spell ){
		if( !playerTurn || !inCombat || M.UI.state != UIStates.Combat )
            return;

        enemy.rend.sortingLayerName = "Back";

        M.stat.inflictCurses();

        spellAlert( spell, () => {
            startCasting(spell);
            M.actionbar.UI.deselectAll();
        });

        playerTurn = false;
    }

    public SpellMissile spellMissile;
    public PlayerSpell currentSpell;

    public void castSpell(){
        // 25% miss chance
        int random = UnityEngine.Random.Range(1,4);
        bool miss = random == 1;

        if(miss) {
            enemy.receiveMiss();
        } else {
            SpellEffects spellEffects = calculateSpellEffects( currentSpell );

            M.stat.receiveHeal( spellEffects.healing );
            enemy.receiveDamage( spellEffects.damage );
            if(spellEffects.damage > 0) {
                enemy.receiveCurses( currentSpell.curses );
            }
        }
		
	}

    public void startCasting(PlayerSpell spell) {
        currentSpell = spell;

        switch( currentSpell.GetType().ToString() ) {
            case "MeleeSpell": M.player.state = PlayerState.Charging; break;
            case "MagicSpell":
                spellMissile.loadSpell( currentSpell as MagicSpell );
                M.player.state = PlayerState.Casting;
            break;
            case "RangeSpell": M.player.state = PlayerState.Ranging; break;
        }
    }

	public void switchTurn(){
		if ( enemy.isDead || M.player.isDead ){
            endCombat();
            return;            
        }
        
        if( !playerTurn ) {
            StartCoroutine(enemy.playTurn());
        }
	}

	public SpellEffects calculateSpellEffects( Spell spell ){

		int damage = 0;
        int healing = 0;

		foreach( SpellStat spellStat in spell.stats ){
			switch( spellStat.effect ) {
				case StatEffect.damage: damage = (int)Mathf.Round(UnityEngine.Random.Range(0f, spellStat.value)); break;
				case StatEffect.healing: healing = (int)Mathf.Round(UnityEngine.Random.Range(0f, spellStat.value)); break;
			}
		}

        if( spell is PlayerSpell ) {

            CombatStatType type = CombatStatType.Strength;
            
            if( spell is RangeSpell ) {
                type = CombatStatType.Range;
            } else if( spell is MagicSpell ) {
                type = CombatStatType.Magic;
            }

            CombatStat stat = M.stat.getStat(type);

            if(enemy.currentDefence > 0)
			    damage *= (int)( stat.currentValue / enemy.currentDefence );
        } else if( spell is EnemySpell ) {
            CombatStat stat = M.stat.getStat(CombatStatType.Defence);
            if(stat.currentValue > 0)
			    damage *= (int)( enemy.currentAttack / stat.currentValue );
        }

		return new SpellEffects( damage, healing );
	}

	public void updateHealthBars(){
		CombatStat HP = M.stat.getStat(CombatStatType.Hitpoints);
		combatUI.playerFrame.healthBar.setHealth ( HP.currentValue, HP.startValue );
        combatUI.enemyFrame.healthBar.setHealth (enemy.combatHitpoints, enemy.startHitpoints );
	}

    public void updateEnemyStats() {
        combatUI.enemyDefence.text = enemy.currentDefence.ToString() + "/" + enemy.startDefence.ToString();
        combatUI.enemyAttack.text = enemy.currentDefence.ToString() + "/" + enemy.startAttack.ToString();
    }
}

public class SpellEffects {
    public SpellEffects( int damage, int healing ) {
        this.damage = damage;
        this.healing = healing;
    }
    public int damage, healing;

}
