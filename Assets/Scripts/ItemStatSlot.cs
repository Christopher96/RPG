﻿using UnityEngine;
using UnityEngine.UI;

public class ItemStatSlot : MonoBehaviour {

	public Image icon;
	public Text value;

    void Start() {
        transform.localScale = Vector3.one;
    }

	public void updateStat ( Sprite _icon, int _value ) {
		icon.sprite = _icon;
		value.text = _value.ToString();
	}
}
