﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Healthbar : MonoBehaviour {

	public Text healthText;
	public Transform healthBars;
	public Transform barPrefab;

	public void setHealth( float currentHealth, float totalHealth ){

		healthText.text = currentHealth + " / " + totalHealth + " HP";
		float percentage = currentHealth / totalHealth;

		foreach( Transform child in healthBars ){
			Destroy( child.gameObject );
		}
		
		for( int i=0; i < Mathf.Ceil( percentage * 10 ); i++ ){
			Transform bar = Instantiate (barPrefab) as Transform;
			bar.SetParent( healthBars );
			bar.localScale = barPrefab.localScale;
			bar.localPosition = barPrefab.localPosition;
		}
	}
}
