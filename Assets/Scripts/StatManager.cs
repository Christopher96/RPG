﻿using System.Collections.Generic;
using UnityEngine;

public class StatManager : MonoBehaviour {

	public List<CombatStat> combatStats;

	public Color32 _buffedColor;
	public Color32 _debuffedColor;

	public static string buffedColor;
	public static string debuffedColor;

	public List<EnemyCurse> curses;
	public List<PlayerBuff> buffs;

	void Awake() {
		buffedColor = Functions.colorToHex(_buffedColor);
		debuffedColor = Functions.colorToHex(_debuffedColor);

		foreach (CombatStat stat in combatStats) {
			stat.currentValue = stat.startValue;
			stat.totalValue = stat.startValue;
			stat.visualBonus = 0;
		}

		curses = new List<EnemyCurse>();
		buffs = new List<PlayerBuff>();
	}

	public void curseUpdate() {
		M.combat.UI.playerFrame.curseList.updateList();
	}

	public CombatStat getStat(CombatStatType type) {
		return combatStats.Find(stat => stat.type == type);
	}

	public void setStat(CombatStatType type, int value) {
		combatStats.Find(stat => stat.type == type).currentValue = value;
	}

	public Sprite getIcon(CombatStatType type) {
		return getStat(type).icon;
	}

	public void resetHP() {
		CombatStat HP = getStat(CombatStatType.Hitpoints);
		HP.currentValue = HP.startValue;
	}

	public void receiveDamage(int amount) {
		M.combat.playerHealthAlert.init(-amount);

		CombatStat HP = getStat(CombatStatType.Hitpoints);

		HP.currentValue -= amount;

		if (HP.currentValue <= 0) {
			HP.currentValue = 0;
			M.player.die();
		}

		M.combat.updateHealthBars();
	}

	public void receiveHeal(int amount) {
		if (amount <= 0)
			return;

		CombatStat HP = getStat(CombatStatType.Hitpoints);

		HP.currentValue += amount;

		if (HP.currentValue >= HP.startValue) {
			HP.currentValue = HP.startValue;
		}

		if (M.combat.inCombat) {
			M.combat.updateHealthBars();
		}
	}

	public void incrementStat(CombatStatType type, int amount) {
		CombatStat stat = getStat(type);

		stat.currentValue += amount;

		if (stat.currentValue >= stat.totalValue) {
			stat.currentValue = stat.totalValue;
		} else if (stat.currentValue < 1) {
			stat.currentValue = 1;
		}
	}

	public void receiveCurses(List<EnemyCurse> _curses) {
		foreach (EnemyCurse curse in _curses) {
			EnemyCurse match = curses.Find(current => current.ID == curse.ID);

			if (match != null) {
				curses.Remove(match);
			}

			curses.Add((EnemyCurse) curse.Clone());
			curseUpdate();

		}
	}

	public void inflictCurses() {
		foreach (EnemyCurse curse in curses) {
			inflictCurse(curse);

			curse.duration--;
			if (curse.duration < 1) {
				curses.Remove(curse);
			}
			curseUpdate();
		}
	}

	public void inflictCurse(EnemyCurse curse) {
		if (curse.type == CombatStatType.Hitpoints) {
			receiveHeal(curse.value);
		} else {
			getStat(curse.type).currentValue -= curse.value;
		}
	}

	public void useConsumable(ConsumableItem item) {
		foreach (ItemCombatStat stat in item.stats) {
			if (stat.type == CombatStatType.Hitpoints) {
				receiveHeal(stat.value);
			} else {
				incrementStat(stat.type, stat.value);
			}
		}

		M.inventory.removeUniqueItem(item);
	}

	public void addStats(List<ItemCombatStat> stats) {
		foreach (ItemCombatStat itemStat in stats) {
			getStat(itemStat.type).totalValue += itemStat.value;
			getStat(itemStat.type).currentValue += itemStat.value;
		}
	}

	public void removeStats(List<ItemCombatStat> stats) {
		foreach (ItemCombatStat itemStat in stats) {
			getStat(itemStat.type).totalValue -= itemStat.value;
			getStat(itemStat.type).currentValue -= itemStat.value;
		}
	}

}
