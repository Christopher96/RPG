﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneSpawn : MonoBehaviour {
	public ZoneSpawnType type;
	Player player;

	void Start() {
		player = FindObjectOfType<Player>();
	}

	public void spawn() {

		Vector3 spawnPos = transform.position;

		switch(type) {
			case ZoneSpawnType.Custom1:
			case ZoneSpawnType.Custom2:
			case ZoneSpawnType.Custom3:
				spawnPos.y -= ( M.player.groundCol.offset.y / 2 );
			break;
			case ZoneSpawnType.Left:
				spawnPos.x -= M.zone.Xoffset;
				spawnPos.y = M.zone.Yoffset;
				M.movement.isFlipped = false;	
			break;
			case ZoneSpawnType.Right:
				spawnPos.x += M.zone.Xoffset;
				spawnPos.y = M.zone.Yoffset;
				M.movement.isFlipped = true;	
			break;
		}

		player.transform.position = spawnPos;
	}
}

public enum ZoneSpawnType {
    Left, Right, Custom1, Custom2, Custom3
}