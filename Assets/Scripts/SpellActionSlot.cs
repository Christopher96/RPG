﻿using UnityEngine;
using UnityEngine.UI;

public class SpellActionSlot : SpellSlot {

    bool isUsable = false;
	public Text questionMark;
    Toggle toggle;

    void Start() {
        toggle = GetComponent<Toggle>();

        toggle.onValueChanged.AddListener( delegate {
			if( toggle.isOn && spell != null && M.UI.state == UIStates.Combat && isUsable ) {
                M.combat.playTurn(spell);
            } 
        });
    }

    // Update is called once per frame
    void Update() {

		if( spell != null ) {
            icon.enabled = true;
			questionMark.enabled = false;
            isUsable = M.spell.canUseSpell(spell);
        } else {
			icon.enabled = false;
			questionMark.enabled = true;
			title.text = "Empty";
            isUsable = false;
        }

        if(!isUsable)
            background.color = Color.grey;
    }

}
