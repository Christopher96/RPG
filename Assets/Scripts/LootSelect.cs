﻿using UnityEngine;
using UnityEngine.UI;

public class LootSelect : MonoBehaviour {

    Item selectedItem;

    public ToggleGroup toggleGroup;

    public Button takeButton;
	public Image icon;
	public Text title;

    public GameObject nothingSelectedObj;
    public GameObject itemSelectedObj;

    public bool selected = false;

    void Start() {
        toggleGroup = GetComponent<ToggleGroup>();

        takeButton.onClick.AddListener( delegate {
			M.loot.lootTable( selectedItem );
        } );
    }

    public void selectItem(Item item) {
        selectedItem = item;
        icon.sprite = item.sprite;
        title.text = item.name;
    }

    void Update() {
        selected = toggleGroup.AnyTogglesOn();
        nothingSelectedObj.SetActive( !selected );
        itemSelectedObj.SetActive( selected );
    }

    public void clear() {
        toggleGroup.SetAllTogglesOff();
        selected = false;
    }
}
