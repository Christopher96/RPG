﻿using UnityEngine;
using UnityEngine.UI;

public class QuestRewardSlot : MonoBehaviour {

    public Text titleSmall;
    public Text titleLarge;
    public Text amountText;
    public Image itemIcon;

    public void instantiate( Item item, int amount ) {

        if( amount > 1 ) {
            titleSmall.enabled = true;
            titleLarge.enabled = false;

            titleSmall.text = item.name;
			titleSmall.color = D.item.getRarityColor( item.rarity );

            amountText.enabled = true;

            amountText.text = "(" + amount.ToString() + ")";
        } else {
            titleSmall.enabled = false;
            titleLarge.enabled = true;

            titleLarge.text = item.name;
			titleLarge.color = D.item.getRarityColor( item.rarity );

            amountText.enabled = false;
        }

        itemIcon.sprite = item.sprite;
    }
}
