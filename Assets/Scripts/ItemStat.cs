﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemCombatStat {
	public CombatStatType type;
	public int value;

	public ItemCombatStat (CombatStatType t, int v){
		type = t;
		value = v;
	}
}