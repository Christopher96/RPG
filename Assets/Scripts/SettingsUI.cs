﻿using UnityEngine;
using UnityEngine.UI;

public class SettingsUI : MonoBehaviour {

	public Toggle joystickToggle;
	
	void Update(){
			joystickToggle.isOn = JoystickController.isLefty;
	}

	public void openSettings() {
		M.UI.state = UIStates.Settings;
	}

	public void toggleJoystick() {
		JoystickController.isLefty = !JoystickController.isLefty;
		M.settings.setBool("settings_joystick", JoystickController.isLefty);
	}
	
}
