﻿using UnityEngine.UI;

public class ButtonFrameSelect : FrameSelect {

	public Button positiveBtn, negativeBtn;
	public string positiveBtnTxt, negativeBtnTxt;

	public virtual void Update(){
		positiveBtn.GetComponentInChildren<Text>().text = positiveBtnTxt;
		negativeBtn.GetComponentInChildren<Text>().text = negativeBtnTxt;
	}
}
