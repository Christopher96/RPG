﻿using System.Collections.Generic;
using UnityEngine;

public class LootList : MonoBehaviour {

    public Transform listItemSlotPrefab;
    public Transform lootContent;

    public GameObject noMoreLoot;

    public void updateLootList() {
        List<Item> table = M.loot.currentChest.table;

        foreach( Transform child in lootContent ) {
            Destroy( child.gameObject );
        }

        foreach( Item item in table ) {
            Transform listItem = Instantiate( listItemSlotPrefab ) as Transform;
            ListItemSlot listItemSlot = listItem.GetComponent<ListItemSlot>();
            listItemSlot.item = item;
            listItemSlot.toggle.group = S.loot.toggleGroup;
            listItemSlot.toggle.onValueChanged.AddListener(delegate {
                    if(listItemSlot.toggle.isOn) {
                        S.loot.selectItem(item);
                    }
            });

            listItem.SetParent( lootContent );
            listItem.localScale = Vector3.one;

            if( item == table[ 0 ] ) {
                Functions.click( listItem );
            }
        }

        noMoreLoot.SetActive( table.Count <= 0 );
    }
}
