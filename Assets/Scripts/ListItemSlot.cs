﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemSlot : ItemSlot {

	public Text title;
	public Transform statList;
	public GameObject statPrefab;
	
	void Update(){
		if(item != null) 
			updateSlot();
	}

	void updateSlot(){

		itemIcon.sprite = item.sprite;
		title.text = item.name;
		title.color = D.item.getRarityColor( item.rarity );

		foreach( Transform child in statList ){
			Destroy( child.gameObject );
		}

		StatItem statItem = item as StatItem;

		if(item is StatItem){
			foreach( ItemCombatStat stat in statItem.stats ) {
				addStat().updateStat( M.stat.getIcon( stat.type ), stat.value );
			}
		}
	}

    ItemStatSlot addStat() {
        GameObject statObject = Instantiate( statPrefab ) as GameObject;
        statObject.transform.SetParent( statList );
        return statObject.GetComponent<ItemStatSlot>();
    }
}
