﻿using System.Collections.Generic;

public class PlayerSpell : Spell {
	public int level = 1;

	public List<PlayerCurse> curses;
	public List<PlayerBuff> buffs;

	public string description;

	public bool isAction {
		get {
			return M.actionbar.spells.Contains( this );
		}
	}

    void Start() {
		if(curses == null) curses = new List<PlayerCurse>();
		if(buffs == null) buffs = new List<PlayerBuff>();

    }
}
 
public enum  PlayerSpellType {
	Arcane, Fire, Frost, Range, Melee
}


