﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerColorSwapper : ColorSwapper {

	public List<PlayerColorSwap> colorSwaps;

	new void Start() {
		base.Start();
		foreach(PlayerColorSwap colorSwap in colorSwaps) {
			swapColor(colorSwap as ColorSwap);
		}
	}

	public void updateColorSwap(ColorSwapType type, Color color) {
		PlayerColorSwap colorSwap = colorSwaps.Find(_colorSwap => _colorSwap.type.Equals(type));
        colorSwap.swapColor = color;

		if(colorSwap != null) {
			swapColor(colorSwap as ColorSwap);
		}
	}


}

[System.Serializable]
public class PlayerColorSwap : ColorSwap {
	public ColorSwapType type;
}

public enum ColorSwapType {
	Head, Chest, Legs, Boots, Skin
}