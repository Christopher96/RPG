﻿using System;
using UnityEngine;

[System.Serializable]
public class Curse : MonoBehaviour, ICloneable {
	[System.NonSerialized]
	public int ID;
	public int value;
	public int duration;
    public Sprite sprite;

	public object Clone() {
		return this.MemberwiseClone();
	}
}
