﻿using UnityEngine;
using UnityEngine.UI;

public class HealthAlert : MonoBehaviour {

    public Image splatter, defence;
    public Text healthtxt;
    Animator anim;

    void Start() {
        anim = GetComponent<Animator>();
    }

    public void init( int amount ) {
        healthtxt.text = "";
        splatter.material.color = Color.white;

        bool isDefence = (amount == 0);
        defence.enabled = isDefence;
        splatter.enabled = !isDefence;
        healthtxt.enabled = !isDefence;

        if( amount > 0 ) {
            healthtxt.text = "+";
            splatter.color = Color.green;
            
        } else if( amount < 0 ) {
            splatter.color = Color.red;
        }

        anim.SetTrigger("fade");

        healthtxt.text += amount.ToString();
    }

    public void miss() {
        healthtxt.enabled = true;
        healthtxt.text = "miss";
        anim.SetTrigger("miss");
    }

    public void stun() {
        anim.SetTrigger("stun");
    }
}
