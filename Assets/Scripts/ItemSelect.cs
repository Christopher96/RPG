﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ItemSelect : MonoBehaviour {

    public bool selected = false;
	public ItemSlot selectedSlot;
    public Item selectedItem;
        
	Item item;
	EquipmentItem equipmentItem;
	ConsumableItem consumableItem;
	Tradable tradable;

	public Alert alert;

	public ToggleGroup toggleGroup;

	NegativeItemAction negativeAction;
	PositiveItemAction positiveAction;

	public ItemFrameSelect frameSelect;
	public string positiveBtnTxt, negativeBtnTxt;

	void Awake(){
		frameSelect.positiveBtn.onClick.AddListener( delegate{
			positiveButtonClick();
		});

		frameSelect.negativeBtn.onClick.AddListener( delegate{
			negativeButtonClick();
		});

		toggleGroup = GetComponent<ToggleGroup>();
	}

	public void deselectAll() {
		selectItem(null);
		toggleGroup.SetAllTogglesOff();
	}

	public void selectItem(Item item) {
		selectedItem = item;
		updateSelect();
	}

	public void updateSelect() {
		selected = selectedItem != null && toggleGroup.AnyTogglesOn();

        if( selected ) {
			
			item = selectedItem;
			equipmentItem = selectedItem as EquipmentItem;
			consumableItem = selectedItem as ConsumableItem;

			if(selectedItem.has<Tradable>()){
				tradable = selectedItem.get<Tradable>();
			}

            positiveAction = PositiveItemAction.Default;
            negativeAction = NegativeItemAction.Default;

            if( M.UI.state == UIStates.Inventory ) {

                negativeAction = NegativeItemAction.DropInventory;

                if( selectedItem is EquipmentItem ) {

					if( M.equipment.isEquiped(equipmentItem) ) {

						positiveBtnTxt = "Unequip";
						if( M.equipment.canUnEquip(equipmentItem) ) {
							positiveAction = PositiveItemAction.UnEquip;
						}

                        negativeBtnTxt = "Drop";
                        negativeAction = NegativeItemAction.DropEquiped;

					} else {

						positiveBtnTxt = "Equip";
						if(M.equipment.canEquip(equipmentItem)){
							positiveAction = PositiveItemAction.Equip;
						}
                        
						negativeBtnTxt = "Drop";
                        negativeAction = NegativeItemAction.DropInventory;
                    }

				} else if( selectedItem is ConsumableItem ) {
					positiveBtnTxt = "Use";
                    positiveAction = PositiveItemAction.Use;

					negativeBtnTxt = "Drop";
					negativeAction = NegativeItemAction.DropInventory;
                } else {
					positiveBtnTxt = "Use";

					negativeBtnTxt = "Drop";
					negativeAction = NegativeItemAction.DropInventory;
				}
            } else if( M.UI.state == UIStates.Shop ) {

                positiveBtnTxt = "Sell";
                negativeBtnTxt = "Buy";

				if( M.inventory.hasUniqueItem(selectedItem) ) {

                    negativeBtnTxt = "Drop";
                    negativeAction = NegativeItemAction.DropInventory;

					if( selectedItem.has<Tradable>() ) {
                        positiveBtnTxt = "Sell\n" + tradable.sellPrice;
                        positiveAction = PositiveItemAction.Sell;
                    }

				} else if( selectedItem.has<Tradable>() ) {
                    negativeBtnTxt = "Buy\n" + tradable.buyPrice;

					if( M.inventory.totalGold >= tradable.buyPrice ) {
                        negativeAction = NegativeItemAction.Buy;
                    }
                }
            }

            frameSelect.positiveBtn.interactable = ( positiveAction != PositiveItemAction.Default );
			frameSelect.negativeBtn.interactable = ( negativeAction != NegativeItemAction.Default );
            frameSelect.updateSelect(item);
		}
	}

	void positiveButtonClick(){

		switch( positiveAction ){

			case PositiveItemAction.Equip :
				M.equipment.equipItem( equipmentItem );
			break;


			case PositiveItemAction.UnEquip:
				M.equipment.unEquipItem( equipmentItem );
            break;

			case PositiveItemAction.Use:
                M.stat.useConsumable( consumableItem );

            break;

			case PositiveItemAction.Sell:
				M.shop.sellItem( tradable );
				selectedItem.Remove();
			break;
		}

		updateSelect();
	}

	void dropCheck( UnityAction<bool> callback ) {
		if (selectedItem.rarity == Rarity.Epic) {
            alert.prompt(
				delegate{ callback( true ); },
				delegate{ callback( false); },
				"Are you sure you want to drop " + selectedItem.name + "?",
                "Drop","Cancel"
            );

		} else {
			callback (true);
		}
    }

	void negativeButtonClick(){

		UIStates prevState = M.UI.state;

		switch( negativeAction ){

		case NegativeItemAction.DropEquiped:
				dropCheck ((drop) => {
					if( drop ) {
	 					M.equipment.removeEquiped(equipmentItem.equipmentType, true);
					}

					M.UI.state = prevState;
				});
            break;

			case NegativeItemAction.DropInventory :
				dropCheck ((drop) => {
					if( drop ) {
						M.inventory.removeItem(item, 1, true);
					}
					
					M.UI.state = prevState;
				});
			break;

			case NegativeItemAction.Buy:
				M.shop.buyItem( item );
			break;
		};

		updateSelect();
	}

}


public enum PositiveItemAction{
	Default, UnEquip, Equip, Use, Sell
}


public enum NegativeItemAction{
	Default, DropEquiped, DropInventory, Buy
}














