﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Enemy : MonoBehaviour {

	[System.NonSerialized]
	public int ID;

	public SpriteRenderer rend;
	public Collider2D groundCol;
	public Collider2D contactCol;

	public int experience;
	public List<Loot> lootTable;

	public Sprite avatar;
	public List<EnemySpell> spellList;
	public int startHitpoints, startDefence, startAttack;

	[System.NonSerialized]
	public int combatHitpoints, currentDefence, currentAttack;

	public float horizontalSpeed = 1;
	public float verticalSpeed = 0.5f;

	public bool isDead, isStunned = false;
	private bool isCharging, isRetreating, isAttacking, isRunning, isCasting, isRanging = false;

	public float yGround;
	private float startX;

	private Animator anim;
	private Rigidbody2D rb;

	public List<PlayerCurse> curses;
	public List<EnemyBuff> buffs;

	void OnTriggerEnter2D(Collider2D col) {
		if (col.tag.Equals("Enemycheck") && !M.combat.inCombat) {
			M.combat.enemyCheck(this);
		}
	}

	void Start() {
		rb = GetComponent<Rigidbody2D>();
		rend = GetComponent<SpriteRenderer>();

		combatHitpoints = startHitpoints;

		startX = transform.position.x;
		anim = GetComponent<Animator>();

		curses = new List<PlayerCurse>();
		buffs = new List<EnemyBuff>();

		currentAttack = startAttack;
		currentDefence = startDefence;
	}

	void FixedUpdate() {
		if (M.combat.inCombat && !isDead) {

			bool playerCheck = contactCol.IsTouching(M.player.contactCol);

			if (isCharging) {
				freeze(playerCheck);

				if (playerCheck) {
					isCharging = false;
					attack();
				} else {
					moveLeft();
				}
			} else if (isRetreating) {

				freeze(transform.position.x >= startX);

				if (transform.position.x >= startX) {
					isRetreating = false;
					switchTurn();
				} else {
					moveRight();
				}
			}
		}
	}

	public void curseUpdate() {
		M.combat.UI.enemyFrame.curseList.updateList();
	}

	void Update() {
		freeze(isDead);

		yGround = groundCol.bounds.center.y;

		Functions.flipGameobject(this.transform, isRetreating);

		anim.SetBool("isRetreating", isRetreating);
		anim.SetBool("isCharging", isCharging);
		anim.SetBool("isRunning", isRunning);
		anim.SetBool("isAttacking", isAttacking);
		anim.SetBool("isCasting", isCasting);
		anim.SetBool("isRanging", isRanging);
		anim.SetBool("isDead", isDead);
	}

	public void normal() {
		groundCol.isTrigger = false;
		curses.Clear();
		buffs.Clear();
	}

	public void freeze(bool doFreeze) {
		rb.constraints = (doFreeze) ? RigidbodyConstraints2D.FreezeAll : RigidbodyConstraints2D.FreezeRotation;
	}

	public void moveRight(float speed = 1f) {
		move(speed, 0);
	}

	public void moveLeft(float speed = 1f) {
		move(-speed, 0);
	}

	private void move(float horizontal, float vertical) {
		rb.AddForce(new Vector2(horizontal * horizontalSpeed, vertical * verticalSpeed));
	}

	void attack() {
		isAttacking = true;
	}

	public void die() {
		isDead = true;
		M.loot.spawnLootTable(transform.position.x, lootTable);
		M.quest.enemyRequirement(this);
	}

	public void retreat() {
		isAttacking = false;
		isRetreating = true;
	}

	void switchTurn() {
		M.combat.playerTurn = true;
		M.combat.switchTurn();
	}

	public EnemySpell currentSpell;

	public IEnumerator playTurn() {
		isStunned = false; 

		if (curses.Any()) {
			inflictCurses();
			yield return new WaitUntil(() => cursesDone);
		}

		if (isDead) {
			M.combat.slowEndCombat();
		} else if(isStunned) {
			switchTurn();
		} else {
			rend.sortingLayerName = "PlayerFront";

			int index = Random.Range(0, spellList.Count - 1);
			EnemySpell spell = spellList[index];

			currentSpell = spell;
			M.combat.spellAlert(spell, () => {
				startCasting();
			});
		}
	}

	public void startCasting() {

		switch (currentSpell.type) {
			case EnemySpellType.Melee:
				isCharging = true;
				break;
			case EnemySpellType.Magic:
				isCasting = true;
				break;
			case EnemySpellType.Range:
				isRanging = true;
				break;
		}
	}

	public void castspell() {
		SpellEffects effects = M.combat.calculateSpellEffects(currentSpell);

		receiveHeal(effects.healing);
		M.stat.receiveDamage(effects.damage);
		if(effects.damage > 0) {
			M.stat.receiveCurses(currentSpell.curses);
		}
	}

	public void receiveCurses(List<PlayerCurse> _curses) {
		foreach (PlayerCurse curse in _curses) {
			PlayerCurse match = curses.Find(current => current.ID == curse.ID);
			if (match != null) {
				curses.Remove(match);
			}

			curses.Add((PlayerCurse) curse.Clone());
			curseUpdate();
		}
	}

	bool cursesDone = false;

	public void inflictCurses() {
		int waitTime = 1;
		cursesDone = false;

		foreach (PlayerCurse curse in curses) {
			StartCoroutine(
				Functions.wait(waitTime, () => {
					inflictCurse(curse);
				})
			);
			waitTime += 1;
		}
	}

	public void inflictCurse(PlayerCurse curse) {
		switch (curse.type) {
			case PlayerCurseType.Attack:
				currentAttack -= curse.value;
				break;
			case PlayerCurseType.Defence:
				currentDefence -= curse.value;
				break;
			case PlayerCurseType.Hitpoints:
				receiveDamage(curse.value);
				break;
			case PlayerCurseType.Stun:
				receiveStun();
				break;
		}
		curse.duration--;
		curseUpdate();

		if (curses.Last() == curse) {
			cursesDone = true;
			curses.RemoveAll(_curse => _curse.duration < 1);
		}
	}

	public void receiveDamage(int amount) {
		M.combat.enemyHealthAlert.init(-amount);
		combatHitpoints -= amount;

		if (combatHitpoints <= 0) {
			combatHitpoints = 0;
			die();
		}
		M.combat.updateHealthBars();
	}

	public void receiveHeal(int amount) {
		if (amount <= 0)
			return;

		M.combat.enemyHealthAlert.init(amount);
		combatHitpoints += amount;

		if (combatHitpoints > startHitpoints) {
			combatHitpoints = startHitpoints;
		}

		M.combat.updateHealthBars();
	}

	public void receiveMiss() {
		M.combat.enemyHealthAlert.miss();
	}

	public void receiveStun() {
		isStunned = true;
		M.combat.enemyHealthAlert.stun();
	}
}

public enum EnemyStatType {
	Defence,
	Attack
}